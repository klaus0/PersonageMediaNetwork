package com.personage_media_network.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.EndorsementMapper;
import com.personage_media_network.entity.Endorsement;
import com.personage_media_network.entity.PageBean;
import com.personage_media_network.httpbean.req.EndorsementReq;

/**
 *  邀请代言、主持
 * @author klaus
 *
 */
@Service
public class EndorsementService {

	@Autowired
	private EndorsementMapper endorsementMapper;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	public ReturnCode listEndorsement(EndorsementReq endorsementReq) {

		PageBean<Endorsement> page = new PageBean<Endorsement>();
		page.setItemTotal(endorsementMapper.selectEndorsementListCount(endorsementReq));
		page.setItems(endorsementMapper.selectEndorsementtionList(endorsementReq));
		return new Builder().code(0).msg("查询成功").object(page).build();
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode saveEndorsement(Endorsement endorsement) {

		endorsementMapper.insertSelective(endorsement);

		return new Builder().code(0).msg("保存成功").object(endorsement).build();

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode updateOrRemoveEndorsement(Endorsement endorsement) {

		endorsementMapper.updateByPrimaryKeySelective(endorsement);

		return new Builder().code(0).msg("操作成功").object(endorsement).build();

	}

	
	
}
