package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 商务合作
 * 
 * @author klaus
 *
 */
public class BusinessCooperation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String businessCooperationId;// 商务合作ID
	@NotBlank(message = "姓名必填")
	private String collaboratorName;// 合作者名称
	@Email(message = "请正确输入邮箱")
	private String collaboratorEamil;// 合作者邮箱
	@Pattern(regexp = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$", message = "请检查手机格式")
	private String collaboratorPhone;// 合作者手机
	@NotBlank(message = "微信必填")
	private String collaboratorWechat;// 合作者微信
	@NotBlank(message = "公司必填")
	private String collaboratorCompany;// 合作者公司
	@NotBlank(message = "合作内容介绍必填")
	private String collaboratorIntroduce;// 合作内容介绍

	private Date createTime;// 创建时间

	private Date updateTime;// 更新时间

	private Boolean isDelete;// 是否删除

	public String getBusinessCooperationId() {
		return businessCooperationId;
	}

	public void setBusinessCooperationId(String businessCooperationId) {
		this.businessCooperationId = businessCooperationId == null ? null : businessCooperationId.trim();
	}

	public String getCollaboratorName() {
		return collaboratorName;
	}

	public void setCollaboratorName(String collaboratorName) {
		this.collaboratorName = collaboratorName == null ? null : collaboratorName.trim();
	}

	public String getCollaboratorEamil() {
		return collaboratorEamil;
	}

	public void setCollaboratorEamil(String collaboratorEamil) {
		this.collaboratorEamil = collaboratorEamil == null ? null : collaboratorEamil.trim();
	}

	public String getCollaboratorPhone() {
		return collaboratorPhone;
	}

	public void setCollaboratorPhone(String collaboratorPhone) {
		this.collaboratorPhone = collaboratorPhone == null ? null : collaboratorPhone.trim();
	}

	public String getCollaboratorWechat() {
		return collaboratorWechat;
	}

	public void setCollaboratorWechat(String collaboratorWechat) {
		this.collaboratorWechat = collaboratorWechat == null ? null : collaboratorWechat.trim();
	}

	public String getCollaboratorCompany() {
		return collaboratorCompany;
	}

	public void setCollaboratorCompany(String collaboratorCompany) {
		this.collaboratorCompany = collaboratorCompany == null ? null : collaboratorCompany.trim();
	}

	public String getCollaboratorIntroduce() {
		return collaboratorIntroduce;
	}

	public void setCollaboratorIntroduce(String collaboratorIntroduce) {
		this.collaboratorIntroduce = collaboratorIntroduce == null ? null : collaboratorIntroduce.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
}