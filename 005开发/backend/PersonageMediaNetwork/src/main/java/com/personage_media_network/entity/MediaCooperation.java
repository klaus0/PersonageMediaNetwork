package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 媒体合作
 * 
 * @author klaus
 *
 */
public class MediaCooperation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mediaCooperationId;// 媒体合作ID
	@NotBlank(message = "合作内容类别必填")
	private String cooperativeContent;// 合作内容类别
	@NotBlank(message = "合作者名称必填")
	private String collaboratorName;// 合作者名称
	@Email(message = "请检查邮箱格式")
	private String collaboratorEmail;// 合作者邮箱
	@Pattern(regexp = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$", message = "请检查手机格式")
	private String collaboratorPhone;// 合作者电话

	private String collaboratorWechat;// 合作者微信
	@NotBlank(message = "公司必填")
	private String collaboratorCompany;// 合作者公司

	private String collaboratorMessage;// 合作者留言

	private Date createTime;// 创建时间

	private Date updateTime;// 更新时间

	private Boolean isDelete;// 是否删除

	public String getMediaCooperationId() {
		return mediaCooperationId;
	}

	public void setMediaCooperationId(String mediaCooperationId) {
		this.mediaCooperationId = mediaCooperationId == null ? null : mediaCooperationId.trim();
	}

	public String getCooperativeContent() {
		return cooperativeContent;
	}

	public void setCooperativeContent(String cooperativeContent) {
		this.cooperativeContent = cooperativeContent == null ? null : cooperativeContent.trim();
	}

	public String getCollaboratorName() {
		return collaboratorName;
	}

	public void setCollaboratorName(String collaboratorName) {
		this.collaboratorName = collaboratorName == null ? null : collaboratorName.trim();
	}

	public String getCollaboratorEmail() {
		return collaboratorEmail;
	}

	public void setCollaboratorEmail(String collaboratorEmail) {
		this.collaboratorEmail = collaboratorEmail == null ? null : collaboratorEmail.trim();
	}

	public String getCollaboratorPhone() {
		return collaboratorPhone;
	}

	public void setCollaboratorPhone(String collaboratorPhone) {
		this.collaboratorPhone = collaboratorPhone == null ? null : collaboratorPhone.trim();
	}

	public String getCollaboratorWechat() {
		return collaboratorWechat;
	}

	public void setCollaboratorWechat(String collaboratorWechat) {
		this.collaboratorWechat = collaboratorWechat == null ? null : collaboratorWechat.trim();
	}

	public String getCollaboratorCompany() {
		return collaboratorCompany;
	}

	public void setCollaboratorCompany(String collaboratorCompany) {
		this.collaboratorCompany = collaboratorCompany == null ? null : collaboratorCompany.trim();
	}

	public String getCollaboratorMessage() {
		return collaboratorMessage;
	}

	public void setCollaboratorMessage(String collaboratorMessage) {
		this.collaboratorMessage = collaboratorMessage == null ? null : collaboratorMessage.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
}