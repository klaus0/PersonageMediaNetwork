package com.personage_media_network.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.Advertisement;
import com.personage_media_network.httpbean.req.AdvertisementManageReq;
import com.personage_media_network.httpbean.req.AdvertisementReq;

@Mapper
public interface AdvertisementMapper {
	int deleteByPrimaryKey(String advertisementId);

	int insert(Advertisement record);

	int insertSelective(Advertisement record);

	Advertisement selectByPrimaryKey(String advertisementId);

	int updateByPrimaryKeySelective(Advertisement record);

	int updateByPrimaryKey(Advertisement record);

	/**
	 * 分页条件查询总条数
	 * @param advertisementReq
	 * @return
	 */
	int selectAdvertisementListCount(AdvertisementReq advertisementReq);

	/**
	 * 分页条件查询
	 * @param advertisementReq
	 * @return
	 */
	List<Advertisement> selectAdvertisementList(AdvertisementReq advertisementReq);
	
	/**
	 * 
	 */
	List<Advertisement> selectAdvertisementShowcaseList();
}