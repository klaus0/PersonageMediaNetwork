package com.personage_media_network.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.InterviewMapper;
import com.personage_media_network.entity.Interview;
import com.personage_media_network.entity.PageBean;

/**
 * 约采访
 * @author klaus
 *
 */
@Service
public class InterviewService {
	@Autowired
	private InterviewMapper interviewMapper;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	public ReturnCode listInterview(Interview interview) {

		PageBean<Interview> page = new PageBean<Interview>();
		page.setItemTotal(interviewMapper.selectInterviewListCount(interview));
		page.setItems(interviewMapper.selectInterviewList(interview));
		return new Builder().code(0).msg("查询成功").object(page).build();
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode saveInterview(Interview interview) {

		interviewMapper.insertSelective(interview);

		return new Builder().code(0).msg("保存成功").object(interview).build();

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode updateOrRemoveInterview(Interview interview) {

		interviewMapper.updateByPrimaryKeySelective(interview);

		return new Builder().code(0).msg("操作成功").object(interview).build();

	}
}
