package com.personage_media_network.httpbean.res;

import java.util.Date;

import com.personage_media_network.entity.Journalism;

public class JournalismContentRes extends Journalism {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String journalismContentId;

    private String journalismId;//资讯ID

    private String journalismContentMedia;//资讯详情媒体路径

    private String journalismContentBackMusic;//资讯详情背景音乐

    private Date createTime;//创建时间

    private Date updateTime;//更新时间

    private Boolean isDelete;//是否删除

    private String journalismContent;//资讯详情内容

	public String getJournalismId() {
		return journalismId;
	}

	public void setJournalismId(String journalismId) {
		this.journalismId = journalismId;
	}

	public String getJournalismContentMedia() {
		return journalismContentMedia;
	}

	public void setJournalismContentMedia(String journalismContentMedia) {
		this.journalismContentMedia = journalismContentMedia;
	}

	public String getJournalismContentBackMusic() {
		return journalismContentBackMusic;
	}

	public void setJournalismContentBackMusic(String journalismContentBackMusic) {
		this.journalismContentBackMusic = journalismContentBackMusic;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getJournalismContent() {
		return journalismContent;
	}

	public void setJournalismContent(String journalismContent) {
		this.journalismContent = journalismContent;
	}

	public String getJournalismContentId() {
		return journalismContentId;
	}

	public void setJournalismContentId(String journalismContentId) {
		this.journalismContentId = journalismContentId;
	}
    

}
