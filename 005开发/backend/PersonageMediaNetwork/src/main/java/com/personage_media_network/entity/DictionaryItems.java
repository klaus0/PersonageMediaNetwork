package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;

/**
 * 字典内容
 * 
 * @author klaus
 *
 */
public class DictionaryItems implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String dictionaryItemId;// 字典内容ID
	@NotBlank(message = "字典内容Pym必填")
	private String dictionaryItemPym;// 字典内容Pym
	@NotBlank(message = "请选择对应字典类型")
	private String dictionaryTypeId;// 字典类型Id
	@NotBlank(message = "字典内容名称必填")
	private String dictionaryItemName;// 字典内容名称
	@NotBlank(message = "字典内容值必填")
	private String dictionaryItemValue;// 字典内容值

	private String dictionaryItemInfo;// 字典内容描述

	private Integer dictionaryItemSort;// 字典内容序号

	private Boolean isDelete;// 是否删除

	private Boolean isEnable;// 是否可用

	private Date createTime;// 创建时间

	private Date updateTime;// 更新时间

	public String getDictionaryItemId() {
		return dictionaryItemId;
	}

	public void setDictionaryItemId(String dictionaryItemId) {
		this.dictionaryItemId = dictionaryItemId == null ? null : dictionaryItemId.trim();
	}

	public String getDictionaryItemPym() {
		return dictionaryItemPym;
	}

	public void setDictionaryItemPym(String dictionaryItemPym) {
		this.dictionaryItemPym = dictionaryItemPym == null ? null : dictionaryItemPym.trim();
	}

	public String getDictionaryTypeId() {
		return dictionaryTypeId;
	}

	public void setDictionaryTypeId(String dictionaryTypeId) {
		this.dictionaryTypeId = dictionaryTypeId == null ? null : dictionaryTypeId.trim();
	}

	public String getDictionaryItemName() {
		return dictionaryItemName;
	}

	public void setDictionaryItemName(String dictionaryItemName) {
		this.dictionaryItemName = dictionaryItemName == null ? null : dictionaryItemName.trim();
	}

	public String getDictionaryItemValue() {
		return dictionaryItemValue;
	}

	public void setDictionaryItemValue(String dictionaryItemValue) {
		this.dictionaryItemValue = dictionaryItemValue == null ? null : dictionaryItemValue.trim();
	}

	public String getDictionaryItemInfo() {
		return dictionaryItemInfo;
	}

	public void setDictionaryItemInfo(String dictionaryItemInfo) {
		this.dictionaryItemInfo = dictionaryItemInfo == null ? null : dictionaryItemInfo.trim();
	}

	public Integer getDictionaryItemSort() {
		return dictionaryItemSort;
	}

	public void setDictionaryItemSort(Integer dictionaryItemSort) {
		this.dictionaryItemSort = dictionaryItemSort;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Boolean getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(Boolean isEnable) {
		this.isEnable = isEnable;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}