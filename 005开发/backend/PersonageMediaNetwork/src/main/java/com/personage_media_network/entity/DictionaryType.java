package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;

/**
 * 字典类型
 * 
 * @author klaus
 *
 */
public class DictionaryType implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String dictionaryTypeId;// 字典类型ID
	@NotBlank(message = "字典类型Pym必填")
	private String dictionaryTypePym;// 字典类型Pym
	@NotBlank(message = "字典类型名称必填")
	private String dictionaryTypeName;// 字典类型名称

	private Integer dictionaryTypeSort;// 字典类型序号

	private String dictionaryTypeInfo;// 字典类型描述
	@NotBlank(message = "是否系统预设必填")
	private Boolean dictionaryTypeIs;// 是否系统预设

	private Boolean isDelete;// 是否删除

	private Boolean isEnable;// 是否可用

	private Date createTime;// 创建时间

	private Date updateTime;// 更新时间

	public String getDictionaryTypeId() {
		return dictionaryTypeId;
	}

	public void setDictionaryTypeId(String dictionaryTypeId) {
		this.dictionaryTypeId = dictionaryTypeId == null ? null : dictionaryTypeId.trim();
	}

	public String getDictionaryTypePym() {
		return dictionaryTypePym;
	}

	public void setDictionaryTypePym(String dictionaryTypePym) {
		this.dictionaryTypePym = dictionaryTypePym == null ? null : dictionaryTypePym.trim();
	}

	public String getDictionaryTypeName() {
		return dictionaryTypeName;
	}

	public void setDictionaryTypeName(String dictionaryTypeName) {
		this.dictionaryTypeName = dictionaryTypeName == null ? null : dictionaryTypeName.trim();
	}

	public Integer getDictionaryTypeSort() {
		return dictionaryTypeSort;
	}

	public void setDictionaryTypeSort(Integer dictionaryTypeSort) {
		this.dictionaryTypeSort = dictionaryTypeSort;
	}

	public String getDictionaryTypeInfo() {
		return dictionaryTypeInfo;
	}

	public void setDictionaryTypeInfo(String dictionaryTypeInfo) {
		this.dictionaryTypeInfo = dictionaryTypeInfo == null ? null : dictionaryTypeInfo.trim();
	}

	public Boolean getDictionaryTypeIs() {
		return dictionaryTypeIs;
	}

	public void setDictionaryTypeIs(Boolean dictionaryTypeIs) {
		this.dictionaryTypeIs = dictionaryTypeIs;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Boolean getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(Boolean isEnable) {
		this.isEnable = isEnable;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}