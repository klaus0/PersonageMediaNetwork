package com.personage_media_network.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.entity.Advertisement;
import com.personage_media_network.httpbean.req.AdvertisementReq;
import com.personage_media_network.service.AdvertisementService;

@RestController
@RequestMapping("admin/advertisement")
public class AdvertisementAdminController {
	@Autowired
	private AdvertisementService advertisementService;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	@GetMapping("listAdvertisement")
	public ReturnCode listAdvertisement(@RequestBody AdvertisementReq advertisementReq) {

		return advertisementService.listAdvertisement(advertisementReq);
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */

	@PostMapping("saveAdvertisement")
	public ReturnCode saveAdvertisement(@RequestBody @Valid Advertisement advertisement, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage()).object(advertisement)
					.build();
		}

		return advertisementService.saveAdvertisement(advertisement);

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PutMapping("updateOrRemoveAdvertisement")
	public ReturnCode updateOrRemoveAdvertisement(@RequestBody @Valid Advertisement advertisement,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage()).object(advertisement)
					.build();
		}
		return advertisementService.updateOrRemoveAdvertisement(advertisement);

	}
}
