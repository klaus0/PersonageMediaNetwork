package com.personage_media_network.httpbean.req;

import com.personage_media_network.entity.Journalism;
import com.personage_media_network.entity.JournalismContent;

public class JournalismContentReq {

	private Journalism journalism;
	private JournalismContent journalismContent;

	public Journalism getJournalism() {
		return journalism;
	}

	public void setJournalism(Journalism journalism) {
		this.journalism = journalism;
	}

	public JournalismContent getJournalismContent() {
		return journalismContent;
	}

	public void setJournalismContent(JournalismContent journalismContent) {
		this.journalismContent = journalismContent;
	}

}
