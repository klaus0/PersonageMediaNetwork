package com.personage_media_network.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.JoinUs;
@Mapper
public interface JoinUsMapper {
    int deleteByPrimaryKey(String joinUsId);

    int insert(JoinUs record);

    int insertSelective(JoinUs record);

    JoinUs selectByPrimaryKey(String joinUsId);

    int updateByPrimaryKeySelective(JoinUs record);

    int updateByPrimaryKeyWithBLOBs(JoinUs record);

    int updateByPrimaryKey(JoinUs record);

	List<JoinUs> selectJoinUsInfo();
}