package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 邀请代言、主持
 * 
 * @author klaus
 *
 */
public class Endorsement implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String endorsementId;// 主持/代言ID
	@NotBlank(message = "活动主题必填")
	private String endorsementTheme;// 主持/代言主题
	@NotBlank(message = "活动类型必填")
	private String endorsementType;// 主持/代言类型
	@NotBlank(message = "姓名必填")
	private String invitationName;// 邀约人姓名
	@Email(message = "请检查邮箱")
	private String invitationEmail;// 邀约人邮箱
	@Pattern(regexp = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$", message = "请检查手机格式")
	private String invitationPhone;// 邀约人手机
	@NotBlank(message = "微信必填")
	private String invitationWechat;// 邀约人微信
	@NotBlank(message = "公司必填")
	private String invitationCompany;// 邀约人公司

	private String endorsementTime;// 希望参与时间

	private String endorsementPlace;// 活动地点

	private String endorsementInfo;// 简介与其他

	private Date createTime;// 创建时间

	private Date updateTime;// 更新时间

	private Boolean isDelete;// 是否删除

	public String getEndorsementId() {
		return endorsementId;
	}

	public void setEndorsementId(String endorsementId) {
		this.endorsementId = endorsementId == null ? null : endorsementId.trim();
	}

	public String getEndorsementTheme() {
		return endorsementTheme;
	}

	public void setEndorsementTheme(String endorsementTheme) {
		this.endorsementTheme = endorsementTheme == null ? null : endorsementTheme.trim();
	}

	public String getEndorsementType() {
		return endorsementType;
	}

	public void setEndorsementType(String endorsementType) {
		this.endorsementType = endorsementType == null ? null : endorsementType.trim();
	}

	public String getInvitationName() {
		return invitationName;
	}

	public void setInvitationName(String invitationName) {
		this.invitationName = invitationName == null ? null : invitationName.trim();
	}

	public String getInvitationEmail() {
		return invitationEmail;
	}

	public void setInvitationEmail(String invitationEmail) {
		this.invitationEmail = invitationEmail == null ? null : invitationEmail.trim();
	}

	public String getInvitationPhone() {
		return invitationPhone;
	}

	public void setInvitationPhone(String invitationPhone) {
		this.invitationPhone = invitationPhone == null ? null : invitationPhone.trim();
	}

	public String getInvitationWechat() {
		return invitationWechat;
	}

	public void setInvitationWechat(String invitationWechat) {
		this.invitationWechat = invitationWechat == null ? null : invitationWechat.trim();
	}

	public String getInvitationCompany() {
		return invitationCompany;
	}

	public void setInvitationCompany(String invitationCompany) {
		this.invitationCompany = invitationCompany == null ? null : invitationCompany.trim();
	}

	public String getEndorsementTime() {
		return endorsementTime;
	}

	public void setEndorsementTime(String endorsementTime) {
		this.endorsementTime = endorsementTime == null ? null : endorsementTime.trim();
	}

	public String getEndorsementPlace() {
		return endorsementPlace;
	}

	public void setEndorsementPlace(String endorsementPlace) {
		this.endorsementPlace = endorsementPlace == null ? null : endorsementPlace.trim();
	}

	public String getEndorsementInfo() {
		return endorsementInfo;
	}

	public void setEndorsementInfo(String endorsementInfo) {
		this.endorsementInfo = endorsementInfo == null ? null : endorsementInfo.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
}