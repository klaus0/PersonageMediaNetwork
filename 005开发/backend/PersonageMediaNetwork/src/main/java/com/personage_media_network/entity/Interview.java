package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 约采访
 * 
 * @author klaus
 *
 */
public class Interview implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String interviewId;// 采访ID
	@NotBlank(message = "姓名必填")
	private String interviewName;// 采访者姓名
	@Email(message = "请检查邮箱格式")
	private String interviewEmail;// 采访者邮箱
	@Pattern(regexp = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$", message = "请检查手机格式")
	private String interviewPhone;// 采访者手机
	@NotBlank(message = "微信必填")
	private String interviewWechat;// 采访者微信
	@NotBlank(message = "被采访对象必填")
	private String intervieweeName;// 被采访对象
	@NotBlank(message = "职务必填")
	private String intervieweeJob;// 被采访对象职务
	@NotBlank(message = "公司名称必填")
	private String interviewCompany;// 公司名称

	private String intervieweeInfo;// 被采访对象描述

	private Date createTime;// 创建时间

	private Date updateTime;// 更新时间

	private Boolean isDelete;// 是否删除

	public String getInterviewId() {
		return interviewId;
	}

	public void setInterviewId(String interviewId) {
		this.interviewId = interviewId == null ? null : interviewId.trim();
	}

	public String getInterviewName() {
		return interviewName;
	}

	public void setInterviewName(String interviewName) {
		this.interviewName = interviewName == null ? null : interviewName.trim();
	}

	public String getInterviewEmail() {
		return interviewEmail;
	}

	public void setInterviewEmail(String interviewEmail) {
		this.interviewEmail = interviewEmail == null ? null : interviewEmail.trim();
	}

	public String getInterviewPhone() {
		return interviewPhone;
	}

	public void setInterviewPhone(String interviewPhone) {
		this.interviewPhone = interviewPhone == null ? null : interviewPhone.trim();
	}

	public String getInterviewWechat() {
		return interviewWechat;
	}

	public void setInterviewWechat(String interviewWechat) {
		this.interviewWechat = interviewWechat == null ? null : interviewWechat.trim();
	}

	public String getIntervieweeName() {
		return intervieweeName;
	}

	public void setIntervieweeName(String intervieweeName) {
		this.intervieweeName = intervieweeName == null ? null : intervieweeName.trim();
	}

	public String getIntervieweeJob() {
		return intervieweeJob;
	}

	public void setIntervieweeJob(String intervieweeJob) {
		this.intervieweeJob = intervieweeJob == null ? null : intervieweeJob.trim();
	}

	public String getInterviewCompany() {
		return interviewCompany;
	}

	public void setInterviewCompany(String interviewCompany) {
		this.interviewCompany = interviewCompany == null ? null : interviewCompany.trim();
	}

	public String getIntervieweeInfo() {
		return intervieweeInfo;
	}

	public void setIntervieweeInfo(String intervieweeInfo) {
		this.intervieweeInfo = intervieweeInfo == null ? null : intervieweeInfo.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
}