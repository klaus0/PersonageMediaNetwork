package com.personage_media_network.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.entity.JoinUs;
import com.personage_media_network.service.JoinUsService;

@RestController
@RequestMapping("admin/joinUs")
public class JoinUsAdminController {
	@Autowired
	private JoinUsService JoinUsService;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	@GetMapping("getJoinUsInfo")
	public ReturnCode getJoinUsInfo() {

		return JoinUsService.getJoinUsInfo();
	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PutMapping("updateJoinUsInfo")
	public ReturnCode updateJoinUsInfo(@RequestBody @Valid JoinUs JoinUs, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage()).object(JoinUs).build();
		}

		return JoinUsService.updateJoinUsInfo(JoinUs);

	}
}
