package com.personage_media_network.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.entity.DictionaryItems;
import com.personage_media_network.service.DictionaryItemsService;
@RestController
@RequestMapping("admin/dictionaryItems")
public class DictionaryItemsAdminController {
	@Autowired
	private DictionaryItemsService dictionaryItemsService;

	/**
	 * 全查字典类型
	 * 
	 * @return
	 */
	@GetMapping("listDictionaryItems")
	public ReturnCode listDictionaryItems(String dictionaryTypeId) {

		return dictionaryItemsService.listDictionaryItems(dictionaryTypeId);
	}

	/**
	 * 保存
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@PostMapping("saveDictionaryItems")
	public ReturnCode saveDictionaryItems(@RequestBody @Valid DictionaryItems dictionaryItems,BindingResult bindingResult
) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage())
					.object(dictionaryItems).build();
		}
		return dictionaryItemsService.updateDictionaryItems(dictionaryItems);
	}

	/**
	 * 根据字典类型Pym查询字典类型
	 * 
	 * @param dictionaryTypePym
	 * @return
	 */
	@GetMapping("ListDictionaryItemsByDictionaryTypePym")
	public ReturnCode ListDictionaryItemsByDictionaryTypePym(String dictionaryTypePym) {

		return dictionaryItemsService.ListDictionaryItemsByDictionaryTypePym(dictionaryTypePym);
	}
}
