package com.personage_media_network.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.AdvertisementManageMapper;
import com.personage_media_network.entity.AdvertisementManage;
import com.personage_media_network.entity.PageBean;
import com.personage_media_network.httpbean.req.AdvertisementManageReq;

/**
 * 广告投放管理
 * 
 * @author klaus
 *
 */
@Service
public class AdvertisementManageService {
	@Autowired
	private AdvertisementManageMapper advertisementManageMapper;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	public ReturnCode listAdvertisementManage(AdvertisementManageReq advertisementManageReq) {

		PageBean<AdvertisementManage> page = new PageBean<AdvertisementManage>();
		page.setItemTotal(advertisementManageMapper.selectAdvertisementManageListCount(advertisementManageReq));
		page.setItems(advertisementManageMapper.selectAdvertisementManageList(advertisementManageReq));
		return new Builder().code(0).msg("查询成功").object(page).build();
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode saveAdvertisementManage(AdvertisementManage advertisementManage) {

		advertisementManageMapper.insertSelective(advertisementManage);

		return new Builder().code(0).msg("保存成功").object(advertisementManage).build();

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode updateOrRemoveAdvertisementManage(AdvertisementManage advertisementManage) {

		advertisementManageMapper.updateByPrimaryKeySelective(advertisementManage);

		return new Builder().code(0).msg("操作成功").object(advertisementManage).build();

	}

}
