package com.personage_media_network.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.entity.BusinessCooperation;
import com.personage_media_network.httpbean.req.BusinessCooperationReq;
import com.personage_media_network.service.BusinessCooperationService;

@RestController
@RequestMapping("admin/businessCooperation")
public class BusinessCooperationAdminController {
	@Autowired
	private BusinessCooperationService businessCooperationService;
	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	@GetMapping("listBusinessCooperation")
	public ReturnCode listBusinessCooperation( @RequestBody BusinessCooperationReq businessCooperationReq) {

			return businessCooperationService.listBusinessCooperation(businessCooperationReq);
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PostMapping("saveBusinessCooperation")
	public ReturnCode saveBusinessCooperation(@RequestBody @Valid BusinessCooperation businessCooperation,BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage())
					.object(businessCooperation).build();
		}


		return businessCooperationService.saveBusinessCooperation(businessCooperation);

	}



	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PutMapping("updateOrRemoveBusinessCooperation")
	public ReturnCode updateOrRemoveBusinessCooperation(@RequestBody @Valid BusinessCooperation businessCooperation,BindingResult bindingResult
) {

		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage())
					.object(businessCooperation).build();
		}


		return businessCooperationService.updateOrRemoveBusinessCooperation(businessCooperation);

	}
}
