package com.personage_media_network.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.JournalismMapper;
import com.personage_media_network.entity.Journalism;
import com.personage_media_network.entity.PageBean;
import com.personage_media_network.httpbean.req.JournalismReq;

/**
 * 资讯
 * @author klaus
 *
 */
@Service
public class JournalismService {

	@Autowired
	private JournalismMapper journalismMapper;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	public ReturnCode listJournalism(JournalismReq journalismReq) {

		PageBean<Journalism> page = new PageBean<Journalism>();
		page.setItemTotal(journalismMapper.selectJournalismListCount(journalismReq));
		page.setItems(journalismMapper.selectJournalismList(journalismReq));
		return new Builder().code(0).msg("查询成功").object(page).build();
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode saveJournalism(Journalism journalism) {

		journalismMapper.insertSelective(journalism);

		return new Builder().code(0).msg("保存成功").object(journalism).build();

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode updateOrRemoveJournalism(Journalism journalism) {

		journalismMapper.updateByPrimaryKeySelective(journalism);

		return new Builder().code(0).msg("操作成功").object(journalism).build();

	}
	
	
}
