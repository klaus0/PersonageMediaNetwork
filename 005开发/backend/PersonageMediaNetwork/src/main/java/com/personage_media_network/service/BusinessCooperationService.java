package com.personage_media_network.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.BusinessCooperationMapper;
import com.personage_media_network.entity.BusinessCooperation;
import com.personage_media_network.entity.PageBean;

/**
 * 商务合作
 * @author klaus
 *
 */
@Service
public class BusinessCooperationService {

	@Autowired
	private BusinessCooperationMapper businessCooperationMapper;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	public ReturnCode listBusinessCooperation(BusinessCooperation businessCooperation) {

		PageBean<BusinessCooperation> page = new PageBean<BusinessCooperation>();
		page.setItemTotal(businessCooperationMapper.selectBusinessCooperationListCount(businessCooperation));
		page.setItems(businessCooperationMapper.selectBusinessCooperationList(businessCooperation));
		return new Builder().code(0).msg("查询成功").object(page).build();
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode saveBusinessCooperation(BusinessCooperation businessCooperation) {

		businessCooperationMapper.insertSelective(businessCooperation);

		return new Builder().code(0).msg("保存成功").object(businessCooperation).build();

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode updateOrRemoveBusinessCooperation(BusinessCooperation businessCooperation) {

		businessCooperationMapper.updateByPrimaryKeySelective(businessCooperation);

		return new Builder().code(0).msg("操作成功").object(businessCooperation).build();

	}

	
}
