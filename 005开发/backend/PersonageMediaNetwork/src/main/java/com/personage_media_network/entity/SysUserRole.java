package com.personage_media_network.entity;

import java.io.Serializable;

/**
 * 用户角色关联表
 * @author klaus
 *
 */
public class SysUserRole  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;//用户角色关联ID

    private String userId;//用户ID

    private String roleId;//角色ID

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }
}