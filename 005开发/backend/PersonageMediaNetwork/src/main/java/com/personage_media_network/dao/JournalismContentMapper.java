package com.personage_media_network.dao;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.JournalismContent;
import com.personage_media_network.httpbean.res.JournalismContentRes;

@Mapper
public interface JournalismContentMapper {
	int deleteByPrimaryKey(String journalismContentId);

	int insert(JournalismContent record);

	int insertSelective(JournalismContent record);

	JournalismContent selectByPrimaryKey(String journalismContentId);

	int updateByPrimaryKeySelective(JournalismContent record);

	int updateByPrimaryKeyWithBLOBs(JournalismContent record);

	int updateByPrimaryKey(JournalismContent record);

	JournalismContentRes getJournalismContent(String journalismId);
}