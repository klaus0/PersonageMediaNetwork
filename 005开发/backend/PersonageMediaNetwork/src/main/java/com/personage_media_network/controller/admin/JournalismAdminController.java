package com.personage_media_network.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.entity.Journalism;
import com.personage_media_network.httpbean.req.JournalismReq;
import com.personage_media_network.service.JournalismService;

@RestController
@RequestMapping("admin/journalism")
public class JournalismAdminController {
	@Autowired
	private JournalismService journalismService;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	@GetMapping("listJournalism")
	public ReturnCode listJournalism( @RequestBody JournalismReq journalismReq) {

		return journalismService.listJournalism(journalismReq);
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PostMapping("saveJournalism")
	public ReturnCode saveJournalism(@RequestBody @Valid Journalism journalism ,BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage())
					.object(journalism).build();
		}


		return journalismService.saveJournalism(journalism);

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PutMapping("updateOrRemoveJournalism")
	public ReturnCode updateOrRemoveJournalism(@RequestBody @Valid Journalism journalism,BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage())
					.object(journalism).build();
		}

		return journalismService.updateOrRemoveJournalism(journalism);

	}
	
}
