package com.personage_media_network.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.MediaCooperation;
@Mapper
public interface MediaCooperationMapper {
    int deleteByPrimaryKey(String mediaCooperationId);

    int insert(MediaCooperation record);

    int insertSelective(MediaCooperation record);

    MediaCooperation selectByPrimaryKey(String mediaCooperationId);

    int updateByPrimaryKeySelective(MediaCooperation record);

    int updateByPrimaryKey(MediaCooperation record);

	int selectMediaCooperationListCount(MediaCooperation mediaCooperation);

	List<MediaCooperation> selectMediaCooperationList(MediaCooperation mediaCooperation);
}