package com.personage_media_network.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.personage_media_network.ReturnCode;

/**
 * @author 作者 fangKai:
 * @version 创建：2017年5月9日 下午6:14:30 类说明
 */
public class Tool {
	/**
	 * MultipartFile 转 file
	 */
	public static File getFile(MultipartFile file) throws Exception {
		File f = null;
		f = File.createTempFile("tmp", null);
		System.out.println(f.getPath());
		file.transferTo(f);
		f.deleteOnExit();
		return f;
	}

	public static int getInt(int a, int b) {
		int c = 0;
		if (a % b != 0) {
			c = a / b + 1;
		} else {
			c = a / b;
		}
		return c;
	}

	/**
	 * 上传文件
	 * 
	 * @param url
	 *            文件存放的路径: 如:/measurfile/tcertificatetype/
	 * @param type
	 *            : 1.上传word类文件 2.word excel pdf 3.图片jpg、png
	 * @param request
	 * @return
	 * @author yfl
	 * @date 2017年7月5日 下午2:10:57
	 */
	public static ReturnCode uploadFile(MultipartFile file, String url, HttpServletRequest request, Integer type) {
		ReturnCode ret = new ReturnCode();
		try {

			String fileNameOld = file.getOriginalFilename();
			// 获取文件的后缀名
			String suffixName = fileNameOld.substring(fileNameOld.lastIndexOf("."));
			if (type == 1) {
				if (!".doc".equalsIgnoreCase(suffixName) && !".docx".equalsIgnoreCase(suffixName)) {
					ret.setSuccess(false);
					ret.setMsg("上传文件类型错误");
					return ret;
				}
			} else if (type == 2) {
				if (!".doc".equalsIgnoreCase(suffixName) && !".docx".equalsIgnoreCase(suffixName)
						&& !".xls".equalsIgnoreCase(suffixName) && !".xlsx".equalsIgnoreCase(suffixName)
						&& !".pdf".equalsIgnoreCase(suffixName)) {
					ret.setSuccess(false);
					ret.setMsg("上传文件类型错误");
					return ret;
				}
			} else if (type == 3) {
				if (!".png".equalsIgnoreCase(suffixName) && !".jpg".equalsIgnoreCase(suffixName)
						&& !".jpeg".equalsIgnoreCase(suffixName)) {
					ret.setSuccess(false);
					ret.setMsg("上传文件类型错误");
					return ret;
				}
			} else if (type == 4) {
				
			} else if (type == 5) {
				if (!".txt".equalsIgnoreCase(suffixName)){
					ret.setSuccess(false);
					ret.setMsg("上传文件类型错误");
					return ret;
				}
			}
			String fileName = Tool.getUUID() + suffixName;
			File cat = new File(request.getSession().getServletContext().getRealPath("/"));
			String Path = cat.getParent() + url;
			File dir = new File(Path);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			String strPath=Path + fileName;
			System.out.println(strPath);
			// 上传文件
			byte[] bytes = file.getBytes();
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(Path + fileName)));
			stream.write(bytes);
			stream.close();
			ret.setObj(url + fileName);
		} catch (Exception e) {
			ret.setSuccess(false);
			ret.setMsg("上传文件异常!");
			return ret;
		}
		return ret;
	}

	/**
	 * 读取配置文件
	 */
	public static Object reflex(String processName, File multipartFile) throws Exception {
		File file = new File("D://测试//spring.txt");
		Properties properties = new Properties();
		properties.load(new FileInputStream(file));

		String className = (String) properties.get("class");

		String mehtod = (String) properties.getProperty("method");

		// 根据类名创建类的对象
		Class<?> cls = Class.forName(className);
		// 根据方法名称获取方法
		Method m = cls.getMethod(mehtod, String.class, File.class);
		// 获取构造器
		Constructor<?> c = cls.getConstructor();
		// 根据构造器，实例化出对象
		Object obj = c.newInstance();
		return m.invoke(obj, processName, multipartFile);
	}

	/**
	 * 分页工具类
	 */
	public static List<Map<String, Object>> getPaging(List<Map<String, Object>> list, Integer start, Integer end) {
		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		for (int i = start; i < end; i++) {
			if (i + 1 > list.size()) {
				break;
			}
			list2.add(list.get(i));
		}
		return list2;
	}

	public static String md5Encode(String inStr) throws UnsupportedEncodingException {
		MessageDigest md5 = null;
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
			return "";
		}

		byte[] byteArray = inStr.getBytes("UTF-8");
		byte[] md5Bytes = md5.digest(byteArray);
		StringBuffer hexValue = new StringBuffer();
		for (int i = 0; i < md5Bytes.length; i++) {
			int val = ((int) md5Bytes[i]) & 0xff;
			if (val < 16) {
				hexValue.append("0");
			}
			hexValue.append(Integer.toHexString(val));
		}
		return hexValue.toString();
	}

	// 获取ip
	public static String getIp(HttpServletRequest request) throws UnknownHostException {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip == null ? "localhost" : ip;
	}

	// 获取时间
	public static Date getTime() {
		return new Date();
	}

	// 判断用户输入的是否为0-9的数字
	public static boolean isNumeric(String str) {
		for (int i = str.length(); --i >= 0;) {
			if (!Character.isDigit(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param 把字符串转成时间
	 * @return 返回时间
	 * @throws Exception
	 */
	public static Date toDate(String dateStr) throws Exception {
		Date date = null;
		SimpleDateFormat formater = new SimpleDateFormat();
		formater.applyPattern("yyyy-MM-dd HH:mm:ss");
		date = formater.parse(dateStr);
		return date;
	}

	/**
	 * @param fangkai
	 * @return 返回某一天是星期几
	 */
	public static int dayForWeek(String pTime) throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(format.parse(pTime));
		int dayForWeek = 0;
		if (c.get(Calendar.DAY_OF_WEEK) == 1) {
			dayForWeek = 7;
		} else {
			dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
		}
		return dayForWeek;
	}

	/**
	 * 返回昨天星期几
	 */
	public static int getTimeString(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if (cal.get(Calendar.DAY_OF_WEEK) - 2 == -1) {
			return 6;
		} else if (cal.get(Calendar.DAY_OF_WEEK) - 2 == 0) {
			return 7;
		} else {
			return cal.get(Calendar.DAY_OF_WEEK) - 2;
		}
	}

	public static Date toDate2(String... strings) {
		Date date = null;
		SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		for (String string : strings) {
			if (string != null || "".equals(string)) {
				try {
					date = formater.parse(string);// 字符串转化成时间格式

				} catch (ParseException e) {// 如果转化失败则返回data

				}
			}
		}

		return date;
	}

	/**
	 * 给当前日期加一天
	 * 
	 * @param fangkai
	 * @return Exception
	 */
	public static String addOneday(String today) {
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date d = new Date(f.parse(today).getTime() + 24 * 3600 * 1000);
			return f.format(d);
		} catch (Exception ex) {
			return "输入格式错误";
		}
	}

	public static String getPYIndexStr(String strChinese, boolean bUpCase) {
		try {

			StringBuffer buffer = new StringBuffer();

			byte b[] = strChinese.getBytes("GBK");// 把中文转化成byte数组

			for (int i = 0; i < b.length; i++) {

				if ((b[i] & 255) > 128) {

					int char1 = b[i++] & 255;

					char1 <<= 8;// 左移运算符用“<<”表示，是将运算符左边的对象，向左移动运算符右边指定的位数，并且在低位补零。其实，向左移n位，就相当于乘上2的n次方

					int chart = char1 + (b[i] & 255);

					buffer.append(getPYIndexChar((char) chart, bUpCase));

					continue;

				}

				char c = (char) b[i];

				if (!Character.isJavaIdentifierPart(c))// 确定指定字符是否可以是 Java
														// 标识符中首字符以外的部分。

					c = 'A';

				buffer.append(c);

			}

			return buffer.toString();

		} catch (Exception e) {

			System.out.println((new StringBuilder()).append("\u53D6\u4E2D\u6587\u62FC\u97F3\u6709\u9519")
					.append(e.getMessage()).toString());

		}

		return null;

	}

	// 将货币类型字符串转成对应的Double类型
	@SuppressWarnings("finally")
	public static Double moneyStringToDouble(String string) {
		Double db = 0.0;
		try {
			StringBuffer sb = new StringBuffer();
			if (string.contains(",")) {
				String[] strArr = string.split(",");
				for (String string2 : strArr) {
					sb.append(string2);
				}
				db = Double.parseDouble(sb.toString());
			} else if (string.contains("，")) {
				String[] strArr = string.split(",");
				for (String string2 : strArr) {
					sb.append(string2);
				}
				db = Double.parseDouble(sb.toString());
			} else {
				db = Double.parseDouble(string);
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			return db;
		}
	}

	/**
	 * 
	 * 得到首字母
	 * 
	 * @param strChinese
	 * 
	 * @param bUpCase
	 * 
	 * @return
	 * 
	 */

	private static char getPYIndexChar(char strChinese, boolean bUpCase) {

		int charGBK = strChinese;

		char result;

		if (charGBK >= 45217 && charGBK <= 45252)

			result = 'A';

		else

		if (charGBK >= 45253 && charGBK <= 45760)

			result = 'B';

		else

		if (charGBK >= 45761 && charGBK <= 46317)

			result = 'C';

		else

		if (charGBK >= 46318 && charGBK <= 46825)

			result = 'D';

		else

		if (charGBK >= 46826 && charGBK <= 47009)

			result = 'E';

		else

		if (charGBK >= 47010 && charGBK <= 47296)

			result = 'F';

		else

		if (charGBK >= 47297 && charGBK <= 47613)

			result = 'G';

		else

		if (charGBK >= 47614 && charGBK <= 48118)

			result = 'H';

		else

		if (charGBK >= 48119 && charGBK <= 49061)

			result = 'J';

		else

		if (charGBK >= 49062 && charGBK <= 49323)

			result = 'K';

		else

		if (charGBK >= 49324 && charGBK <= 49895)

			result = 'L';

		else

		if (charGBK >= 49896 && charGBK <= 50370)

			result = 'M';

		else

		if (charGBK >= 50371 && charGBK <= 50613)

			result = 'N';

		else

		if (charGBK >= 50614 && charGBK <= 50621)

			result = 'O';

		else

		if (charGBK >= 50622 && charGBK <= 50905)

			result = 'P';

		else

		if (charGBK >= 50906 && charGBK <= 51386)

			result = 'Q';

		else

		if (charGBK >= 51387 && charGBK <= 51445)

			result = 'R';

		else

		if (charGBK >= 51446 && charGBK <= 52217)

			result = 'S';

		else

		if (charGBK >= 52218 && charGBK <= 52697)

			result = 'T';

		else

		if (charGBK >= 52698 && charGBK <= 52979)

			result = 'W';

		else

		if (charGBK >= 52980 && charGBK <= 53688)

			result = 'X';

		else

		if (charGBK >= 53689 && charGBK <= 54480)

			result = 'Y';

		else

		if (charGBK >= 54481 && charGBK <= 55289)

			result = 'Z';

		else

			result = (char) (65 + (new Random()).nextInt(25));

		if (!bUpCase)

			result = Character.toLowerCase(result);

		return result;

	}

	public static String getDateString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String str = sdf.format(date);
		return str;
	}

	/**
	 * 两个时间相差距离多少天多少小时多少分多少秒
	 * 
	 * @param str1
	 *            时间参数 1 格式：1990-01-01 12:00:00
	 * @param str2
	 *            时间参数 2 格式：2009-01-01 12:00:00
	 * @return String 返回值为：xx天xx小时xx分xx秒
	 */
	public static long getDistanceTime(String str1, String str2) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date one;
		Date two;
		long day = 0;
		try {
			one = df.parse(str1);
			two = df.parse(str2);
			long time1 = one.getTime();
			long time2 = two.getTime();
			long diff;
			if (time1 < time2) {
				diff = time2 - time1;
			} else {
				diff = time1 - time2;
			}
			day = diff / (24 * 60 * 60 * 1000);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return day;
	}

	/**
	 * 测试主函数
	 * 
	 * @param fangkai
	 * @throws UnknownHostException
	 * @throws Exception
	 */
	private static final String GB_2312 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbp"
			+ "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbpbbbbbbbbbbbbbbbbbb"
			+ "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
			+ "pbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
			+ "bbbbbbbbbbbbbbbbbbbbcccccccccccccccccccccccccccccc"
			+ "ccccccccccccccccccccccccccccccccccczcccccccccccccc"
			+ "ccccccccccccccccccccccccccccccccccccsccccccccccccc"
			+ "cccccccccccccccccccccccccccccccccccccccccczccccccc"
			+ "cccccccccccccccccccccccccccccccccccccccccccccccccc"
			+ "cccddddddddddddddddddddddddddddddddddddddddddddddd"
			+ "dddddddddddddddddddddzdddddddddddddddddddddddddddd"
			+ "dddddddddddddddddddddddddddddddtdddddddddddddddddd"
			+ "dddddddddddddddddddddddddddddddddddddeeeeeeeeeeeee"
			+ "eeeeeeeeefffffffffffffffffffffffffffffffffffffffff"
			+ "ffffffffffffffffffffffffffffffffffffffffffffffffff"
			+ "fffffffffffffpffffffffffffffffffffgggggggggggggggg"
			+ "ggggggggggggggggggghggggggggggggghgggggggggggggggg"
			+ "gggggggggggggggggggggggggggggggggggggggggggggggggg"
			+ "ggggggggggggggggggggggggggggggggggggggghhhhhhhhhhh"
			+ "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhmhhhhhhhhhhh"
			+ "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"
			+ "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"
			+ "hhhhhhhhhhhhhhhhhhhhjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj"
			+ "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj"
			+ "jjjjjjjjjjjjjjkjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj"
			+ "jjjjjjjyjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj"
			+ "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj"
			+ "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj"
			+ "jjjjjjjjjjjjjjjkkkgkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkh"
			+ "kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk"
			+ "kkkkkkkkkkkkkkklllllllllllllllllllllllllllllllllll"
			+ "llllllllllllllllllllllllllllllllllllllllllllllllll"
			+ "llllllllllllllllllllllllllllllllllllllllllllllllll"
			+ "llllllllllllllllllllllllllllllllllllllllllllllllll"
			+ "llllllllllllllllllllllllllllllllllllllllllllllllll"
			+ "lllllllllllllmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"
			+ "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"
			+ "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"
			+ "mmmmmmmmmmmmmmnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn"
			+ "nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnooooo"
			+ "oooppppppppppppppppppppppppppppppppppppppppppppppp"
			+ "pppppppppppppppppppppppppppppppppppppppppppppppppp"
			+ "ppppppppppppppppppppppppbqqqqqqqqqqqqqqqqqqqqqqqqq"
			+ "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
			+ "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"
			+ "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqrrrrrrrrrrrrrrrrrr"
			+ "rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrsssssssss"
			+ "ssssssssssssssssssssssssssssssssssssssssssssssssss"
			+ "ssssssssssssssssssssssssssssssssssssssssssssssssss"
			+ "ssssssssssssssssssssssssssssssssssssssssssssssssss"
			+ "ssssssssssssssssssssssssssssssssssssssssssssssssss"
			+ "sssssssssssssssssssssssssssssssssssssssssssssssssx"
			+ "sssssssssssssssssssssssssssttttttttttttttttttttttt"
			+ "tttttttttttttttttttttttttttttttttttttttttttttttttt"
			+ "tttttttttttttttttttttttttttttttttttttttttttttttttt"
			+ "tttttttttttttttttttttttttttttttttwwwwwwwwwwwwwwwww"
			+ "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
			+ "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
			+ "wwwxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxsx"
			+ "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
			+ "xxxxxxxxxxxxxxxxxxxxxjxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
			+ "xxxxxhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxcxxxxxxxxx"
			+ "xxxxxxxxxxxxxxxxxxxxxxxxxxyyyyyyyyyyyyyyyyyyyyyyyy"
			+ "yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"
			+ "yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"
			+ "yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"
			+ "yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"
			+ "yyyyyyyyyyyyyyyyyyyyyyyyxyyyyyyyyyyyyyyyyyyyyyyyyy"
			+ "yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyzzzzzzzzzzzzzzzzzz"
			+ "zzzzzzzzzzzzzzzzzzzzzczzzzzzzzzzzzzzzzzzzzzzzzzzzz"
			+ "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"
			+ "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"
			+ "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"
			+ "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"
			+ "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"
			+ "zzzzz     cjwgnspgcgnesypbtyyzdxykygtdjnnjqmbsjzsc"
			+ "yjsyyfpgkbzgylywjkgkljywkpjqhytwddzlsymrypywwcckzn"
			+ "kyygttngjnykkzytcjnmcylqlypysfqrpzslwbtgkjfyxjwzlt"
			+ "bncxjjjjtxdttsqzycdxxhgckbphffsswybgmxlpbylllhlxst"
			+ "zmyjhsojnghdzqyklgjhsgqzhxqgkxzzwyscscjxyeyxadzpmd"
			+ "ssmzjzqjyzcdjzwqjbyzbjgznzcpwhwxhqkmwfbpbydtjzzkxx"
			+ "ylygxfptyjyyzpszlfchmqshgmxxsxjyqdcsbbqbefsjyhxwgz"
			+ "kpylqbgldlcdtnmaeddkssngycsgxlyzaypnptsdkdylhgymyl"
			+ "cxpycjndqjwxqxfyyfjlejpzrxccqwqqsbzkymgplbmjrqcfln"
			+ "ymyqmtqyrbcjthztqfrxqhxmqjcjlyxgjmshzkbswyemyltxfs"
			+ "ydsglycjqxsjnqbsctyhbftdcyjdjyyghqfsxwckqkxebptlpx"
			+ "jzsrmebwhjlpjslyysmdxlclqkxlhxjrzjmfqhxhwywsbhtrxx"
			+ "glhqhfnmgykldyxzpylggtmtcfpnjjzyljtyanjgbjplqgszyq"
			+ "yaxbkysecjsznslyzhzxlzcghpxzhznytdsbcjkdlzayfmytle"
			+ "bbgqyzkggldndnyskjshdlyxbcgyxypkdjmmzngmmclgezszxz"
			+ "jfznmlzzthcsydbdllscddnlkjykjsycjlkwhqasdknhcsgaeh"
			+ "daashtcplcpqybsdmpjlpzjoqlcdhjxysprchnwjnlhlyyqyhw"
			+ "zptczgwwmzffjqqqqyxaclbhkdjxdgmmydqxzllsygxgkjrywz"
			+ "wyclzmssjzldbydcpcxyhlxchyzjqsfqagmnyxpfrkssbjlyxy"
			+ "syglnscmhcwwmnzjjlxxhchsyzsttxrycyxbyhcsmxjsznpwgp"
			+ "xxtaybgajcxlysdccwzocwkccsbnhcpdyznfcyytyckxkybsqk"
			+ "kytqqxfcwchcykelzqbsqyjqcclmthsywhmktlkjlycxwheqqh"
			+ "tqkjpqsqscfymmdmgbwhwlgsllystlmlxpthmjhwljzyhzjxht"
			+ "xjlhxrswlwzjcbxmhzqxsdzpsgfcsglsxymqshxpjxwmyqksmy"
			+ "plrthbxftpmhyxlchlhlzylxgsssstclsldclrpbhzhxyyfhbb"
			+ "gdmycnqqwlqhjjzywjzyejjdhpblqxtqkwhlchqxagtlxljxms"
			+ "ljhtzkzjecxjcjnmfbycsfywybjzgnysdzsqyrsljpclpwxsdw"
			+ "ejbjcbcnaytwgmpapclyqpclzxsbnmsggfnzjjbzsfzyndxhpl"
			+ "qkzczwalsbccjxjyzgwkypsgxfzfcdkhjgxtlqfsgdslqwzkxt"
			+ "mhsbgzmjzrglyjbpmlmsxlzjqzhzyjczydjwfmjklddpmjegxy"
			+ "hylxhlqyqhkycwcjmyyxnatjhyccxzpcqlbzwwytwsqcmlpmyr"
			+ "jcccxfpznzzljplxxyztzlgdltcklyrzzgqtkjhhgjljaxfgfj"
			+ "zslcfdqzlclgjdjcsnzlljpjqdcclcjxmyzftsxgcgsbrzxjqq"
			+ "ctzhgyqtjqqlzxjylylncyamcstylpdjbyregklzyzhlyszqlz"
			+ "nwczcllwjqjjjkdgjzolbbzppglghtgzxyjhzmycnqsycyhbhg"
			+ "xkamtxyxnbskyzzgjzlqjtfcjxdygjqjjpmgwgjjjpkqsbgbmm"
			+ "cjssclpqpdxcdyykyfcjddyygywrhjrtgznyqldkljszzgzqzj"
			+ "gdykshpzmtlcpwnjyfyzdjcnmwescyglbtzcgmssllyxqsxxbs"
			+ "jsbbsgghfjlypmzjnlyywdqshzxtyywhmcyhywdbxbtlmsyyyf"
			+ "sxjchtxxlhjhfssxzqhfzmzcztqcxzxrttdjhnnyzqqmtqdmmz"
			+ " ytxmjgdxcdyzbffallztdltfxmxqzdngwqdbdczjdxbzgsqqd"
			+ "djcmbkzffxmkdmdsyyszcmljdsynsprskmkmpcklgdbqtfzswt"
			+ "fgglyplljzhgjjgypzltcsmcnbtjbqfkdhpyzgkpbbymtdssxt"
			+ "bnpdkleycjnyddykzddhqhsdzsctarlltkzlgecllkjlqjaqnb"
			+ "dkkghpjxzqksecshalqfmmgjnlyjbbtmlyzxdxjpldlpcqdhzy"
			+ "cbzsczbzmsljflkrzjsnfrgjhxpdhyjybzgdlqcsezgxlblhyx"
			+ "twmabchecmwyjyzlljjyhlgbdjlslygkdzpzxjyyzlwcxszfgw"
			+ "yydlyhcljscmbjhblyzlycblydpdqysxqzbytdkyxlyycnrjmp"
			+ "dqgklcljbcxbjddbblblczqrppxjcjlzcshltoljnmdddlngka"
			+ "thqhjhykheznmshrphqqjchgmfprxhjgdychgklyrzqlcyqjnz"
			+ "sqtkqjymszxwlcfqqqxyfggyptqwlmcrnfkkfsyylybmqammmy"
			+ "xctpshcptxxzzsmphpshmclmldqfyqxszyjdjjzzhqpdszglst"
			+ "jbckbxyqzjsgpsxqzqzrqtbdkwxzkhhgflbcsmdldgdzdblzyy"
			+ "cxnncsybzbfglzzxswmsccmqnjqsbdqsjtxxmbltxcclzshzcx"
			+ "rqjgjylxzfjphymzqqydfqjqlzznzjcdgzygztxmzysctlkpht"
			+ "xhtlbjxjlxscdqxcbbtjfqzfsltjbtkqbxxjjljchczdbzjdcz"
			+ "jdcprnpqcjpfczlclzxzdmxmphjsgzgszzqlylwtjpfsyaxmcj"
			+ "btzyycwmytzsjjlqcqlwzmalbxyfbpnlsfhtgjwejjxxglljst"
			+ "gshjqlzfkcgnndszfdeqfhbsaqtgylbxmmygszldydqmjjrgbj"
			+ "tkgdhgkblqkbdmbylxwcxyttybkmrtjzxqjbhlmhmjjzmqasld" + "cyxyqdlqcafywyxqhz";
	private static final String GBK_3 = "ksxsm sdqlybjjjgczbjfya jhphsyzgj   sn      xy  ng"
			+ "    lggllyjds yssgyqyd xjyydldwjjwbbftbxthhbczcrfm"
			+ "qwyfcwdzpyddwyxjajpsfnzyjxxxcxnnxxzzbpysyzhmzbqbzc"
			+ "ycbxqsbhhxgfmbhhgqcxsthlygymxalelccxzrcsd njjtzzcl"
			+ "jdtstbnxtyxsgkwyflhjqspxmxxdc lshxjbcfybyxhczbjyzl"
			+ "wlcz gtsmtzxpqglsjfzzlslhdzbwjncjysnycqrzcwybtyftw"
			+ "ecskdcbxhyzqyyxzcffzmjyxxsdcztbzjwszsxyrnygmdthjxs"
			+ "qqccsbxrytsyfbjzgclyzzbszyzqscjhzqydxlbpjllmqxtydz"
			+ "sqjtzplcgqtzwjbhcjdyfxjelbgxxmyjjqfzasyjnsydk jcjs"
			+ "zcbatdclnjqmwnqncllkbybzzsyhjqltwlccxthllzntylnzxd"
			+ "dtcenjyskkfksdkghwnlsjt jymrymzjgjmzgxykymsmjklfxm"
			+ "tghpfmqjsmtgjqdgyalcmzcsdjlxdffjc f  ffkgpkhrcjqcj"
			+ "dwjlfqdmlzbjjscgckdejcjdlzyckscclfcq czgpdqzjj hdd"
			+ "wgsjdkccctllpskghzzljlgjgjjtjjjzczmlzyjkxzyzmljkyw"
			+ "xmkjlkjgmclykjqlblkmdxwyxysllpsjqjqxyqfjtjdmxxllcr"
			+ "qyjb xgg pjygegdjgnjyjkhqfqzkhyghdgllsdjjxkyoxnzsx"
			+ "wwxdcskxxjyqscsqkjexsyzhydz ptqyzmtstzfsyldqagylcq"
			+ "lyyyhlrq ldhsssadsjbrszxsjyrcgqc hmmxzdyohycqgphhy"
			+ "nxrhgjlgwqwjhcstwasjpmmrdsztxyqpzxyhyqxtpbfyhhdwzb"
			+ "txhqeexzxxkstexgltxydn  hyktmzhxlplbmlsfhyyggbhyqt"
			+ "xwlqczydqdq gd lls zwjqwqajnytlxanzdecxzwwsgqqdyzt"
			+ "chyqzlxygzglydqtjtadyzzcwyzymhyhyjzwsxhzylyskqysbc"
			+ "yw  xjzgtyxqsyhxmchrwjpwxzlwjs sgnqbalzzmtjcjktsax"
			+ "ljhhgoxzcpdmhgtysjxhmrlxjkxhmqxctxwzbkhzccdytxqhlx"
			+ "hyx syydz znhxqyaygypdhdd pyzndltwxydpzjjcxmtlhbyn"
			+ "yymhzllhnmylllmdcppxmxdkycydltxchhznaclcclylzsxzjn"
			+ "zln lhyntkyjpychegttgqrgtgyhhlgcwyqkpyyyttttlhylly"
			+ "ttplkyzqqzdq  nmjzxyqmktfbjdjjdxbtqzgtsyflqgxblzfh"
			+ " zadpmjhlccyhdzfgydgcyxs hd d axxbpbyyaxcqffqyjxdl"
			+ "jjzl bjydyqszwjlzkcdtctbkdyzdqjnkknjgyeglfykasntch"
			+ "blwzbymjnygzyheyfjmctyfzjjhgck lxhdwxxjkyykssmwctq"
			+ "zlpbzdtwzxzag kwxl lspbclloqmmzslbczzkdcz xgqqdcyt"
			+ "zqwzqssfpktfqdcdshdtdwfhtdy jaqqkybdjyxtlj drqxxxa"
			+ "ydrjlklytwhllrllcxylbw z  zzhkhxksmdsyyjpzbsqlcxxn"
			+ "xwmdq gqmmczjgttybhyjbetpjxdqhkzbhfdxkawtwajldyjsf"
			+ "hblddqjncxfjhdfjjwzpkzypcyzynxff ydbzznytxzembsehx"
			+ "fzmbflzrsymzjrdjgxhjgjjnzzxhgxhymlpeyyxtgqshxssxmf"
			+ "mkcctxnypszhzptxwywxyysljsqxzdleelmcpjclxsqhfwwtff"
			+ "tnqjjjdxhwlyznflnkyyjldx hdynrjtywtrmdrqhwqcmfjdyz"
			+ "hmyyxjwzqtxtlmrspwwchjb xygcyyrrlmpymkszyjrmysntpl"
			+ "nbpyyxmykyngjzznlzhhanmpgwjdzmxxmllhgdzxyhxkrycjmf"
			+ "fxyhjfssqlxxndyca nmtcjcyprrnytyqym sxndlylyljnlxy"
			+ "shqmllyzljzxstyzsmcqynzlxbnnylrqtryyjzzhsytxcqgxzs"
			+ "shmkczyqhzjnbh qsnjnzybknlqhznswxkhjyybqlbfl p bkq"
			+ "zxsddjmessmlxxkwnmwwwydkzggtggxbjtdszxnxwmlptfxlcx"
			+ "jjljzxnwxlyhhlrwhsc ybyawjjcwqqjzzyjgxpltzftpakqpt"
			+ "lc  xtx hklefdleegqymsawhmljtwyqlyjeybqfnlyxrdsctg"
			+ "gxyyn kyqctlhjlmkkcgygllldzydhzwpjzkdyzzhyyfqytyzs"
			+ "ezzlymhjhtwyzlkyywzcskqqtdxwctyjklwqbdqyncs szjlkc"
			+ "dcdtlzzacqqzzddxyplxzbqjylzllqdzqgyjyjsyxnyyynyjxk"
			+ "xdazwrdljyyynjlxllhxjcykynqcclddnyyykyhhjcl pb qzz"
			+ "yjxj fzdnfpzhddwfmyypqjrssqzsqdgpzjwdsjdhzxwybp gp"
			+ "tmjthzsbgzmbjczwbbzmqcfmbdmcjxljbgjtz mqdyxjzyctyz"
			+ "tzxtgkmybbcljssqymscx jeglxszbqjjlyxlyctsxmcwfa kb"
			+ "qllljyxtyltxdphnhfqyzyes sdhwdjbsztfd czyqsyjdzjqp"
			+ "bs j fbkjbxtkqhmkwjjlhhyyyyywyycdypczyjzwdlfwxwzzj"
			+ "cxcdjzczlxjjtxbfwpxzptdzbccyhmlxbqlrtgrhqtlf mwwjx"
			+ "jwcysctzqhxwxkjybmpkbnzhqcdtyfxbyxcbhxpsxt m sxlhk"
			+ "mzxydhwxxshqhcyxglcsqypdh my ypyyykzljqtbqxmyhcwll"
			+ "cyl ewcdcmlggqktlxkgndgzyjjlyhqdtnchxwszjydnytcqcb"
			+ "hztbxwgwbxhmyqsycmqkaqyncs qhysqyshjgjcnxkzycxsbxx"
			+ "hyylstyxtymgcpmgcccccmztasgqzjlosqylstmqsqdzljqqyp"
			+ "lcycztcqqpbqjclpkhz yyxxdtddsjcxffllxmlwcjcxtspyxn"
			+ "dtjsjwxqqjskyylsjhaykxcyydmamdqmlmczncybzkkyflmcsc"
			+ "lhxrcjjgslnmtjzzygjddzjzk qgjyyxzxxqhheytmdsyyyqlf"
			+ " zzdywhscyqwdrxqjyazzzdywbjwhyqszywnp  azjbznbyzzy"
			+ "hnscpjmqcy zpnqtbzjkqqhngccxchbzkddnzhjdrlzlsjljyx"
			+ "ytbgtcsqmnjpjsrxcfjqhtpzsyjwbzzzlstbwwqsmmfdwjyzct"
			+ "bwzwqcslqgdhqsqlyzlgyxydcbtzkpj gm pnjkyjynhpwsnsz"
			+ "zxybyhyzjqjtllcjthgdxxqcbywbwzggqrqzssnpkydznxqxjm"
			+ "y dstzplthzwxwqtzenqzw ksscsjccgptcslccgllzxczqthn"
			+ "jgyqznmckcstjskbjygqjpldxrgzyxcxhgdnlzwjjctsbcjxbf"
			+ "zzpqdhjtywjynlzzpcjdsqjkdxyajyemmjtdljyryynhjbngzj"
			+ "kmjxltbsllrzylcscnxjllhyllqqqlxymswcxsljmc zlnsdwt"
			+ "jllggjxkyhbpdkmmscsgxjcsdybxdndqykjjtxdygmzzdzslo "
			+ "yjsjzdlbtxxxqqjzlbylwsjjyjtdzqqzzzzjlzcdzjhpl qplf"
			+ "fjzysj zfpfzksyjjhxttdxcysmmzcwbbjshfjxfqhyzfsjybx"
			+ "pzlhmbxhzxfywdab lktshxkxjjzthgxh jxkzxszzwhwtzzzs"
			+ "nxqzyawlcwxfxyyhxmyyswqmnlycyspjkhwcqhyljmzxhmcnzh"
			+ "hxcltjplxyjhdyylttxfszhyxxsjbjyayrmlckd yhlrlllsty"
			+ "zyyhscszqxkyqfpflk ntljmmtqyzwtlll s rbdmlqjbcc qy"
			+ "wxfzrzdmcyggzjm  mxyfdxc shxncsyjjmpafyfnhyzxyezy "
			+ "sdl zztxgfmyyysnbdnlhpfzdcyfssssn zzdgpafbdbzszbsg"
			+ "cyjlm  z yxqcyxzlckbrbrbzcycjzeeyfgzlyzsfrtkqsxdcm"
			+ "z  jl xscbykjbbrxllfqwjhyqylpzdxczybdhzrbjhwnjtjxl"
			+ "kcfssdqyjkzcwjl b  tzlltlqblcqqccdfpphczlyygjdgwcf"
			+ "czqyyyqyrqzslszfcqnwlhjcjjczkypzzbpdc   jgx gdz  f"
			+ "gpsysdfwwjzjyxyyjyhwpbygxrylybhkjksftzmmkhtyysyyzp"
			+ "yqydywmtjjrhl   tw  bjycfnmgjtysyzmsjyjhhqmyrszwtr"
			+ "tzsskx gqgsptgcznjjcxmxgzt ydjz lsdglhyqgggthszpyj"
			+ "hhgnygkggmdzylczlxqstgzslllmlcskbljzzsmmytpzsqjcj "
			+ " zxzzcpshkzsxcdfmwrllqxrfzlysdctmxjthjntnrtzfqyhqg"
			+ "llg   sjdjj tqjlnyhszxcgjzypfhdjspcczhjjjzjqdyb ss"
			+ "lyttmqtbhjqnnygjyrqyqmzgcjkpd gmyzhqllsllclmholzgd"
			+ "yyfzsljc zlylzqjeshnylljxgjxlyjyyyxnbzljsszcqqzjyl"
			+ "lzldj llzllbnyl hxxccqkyjxxxklkseccqkkkcgyyxywtqoh"
			+ "thxpyxx hcyeychbbjqcs szs lzylgezwmysx jqqsqyyycmd"
			+ "zywctjsycjkcddjlbdjjzqysqqxxhqjohdyxgmajpchcpljsmt"
			+ "xerxjqd pjdbsmsstktssmmtrzszmldj rn sqxqydyyzbdsln"
			+ "fgpzmdycwfdtmypqwytjzzqjjrjhqbhzpjhnxxyydyhhnmfcpb"
			+ "zpzzlzfmztzmyftskyjyjzhbzzygh pzcscsjssxfjgdyzyhzc"
			+ "whcsexfqzywklytmlymqpxxskqjpxzhmhqyjs cjlqwhmybdhy"
			+ "ylhlglcfytlxcjscpjskphjrtxteylssls yhxscznwtdwjslh"
			+ "tqdjhgydphcqfzljlzptynlmjllqyshhylqqzypbywrfy js y"
			+ "p yrhjnqtfwtwrchygmm yyhsmzhngcelqqmtcwcmpxjjfyysx"
			+ "ztybmstsyjdtjqtlhynpyqzlcxznzmylflwby jgsylymzctdw"
			+ "gszslmwzwwqzsayysssapxwcmgxhxdzyjgsjhygscyyxhbbzjk"
			+ "ssmalxycfygmqyjycxjlljgczgqjcczotyxmtthlwtgfzkpzcx"
			+ "kjycxctjcyh xsgckxzpsjpxhjwpjgsqxxsdmrszzyzwsykyzs"
			+ "hbcsplwsscjhjlchhylhfhhxjsx lnylsdhzxysxlwzyhcldyh"
			+ "zmdyspjtqznwqpsswctst zlmssmnyymjqjzwtyydchqlxkwbg"
			+ "qybkfc jdlzllyylszydwhxpsbcmljscgbhxlqrljxysdwxzsl"
			+ "df hlslymjljylyjcdrjlfsyjfnllcqyqfjy szlylmstdjcyh"
			+ "zllnwlxxygyygxxhhzzxczqzfnwpypkpypmlgxgg dxzzkzfbx"
			+ "xlzptytswhzyxhqhxxxywzyswdmzkxhzphgchj lfjxptzthly"
			+ "xcrhxshxkjxxzqdcqyl jlkhtxcwhjfwcfpqryqxyqy gpggsc"
			+ "sxngkchkzxhflxjbyzwtsxxncyjjmwzjqrhfqsyljzgynslgtc"
			+ "ybyxxwyhhxynsqymlywgyqbbzljlpsytjzhyzwlrorjkczjxxy"
			+ "xchdyxyxxjddsqfxyltsfxlmtyjmjjyyxltcxqzqhzlyyxzh n"
			+ "lrhxjcdyhlbrlmrllaxksllljlxxxlycry lccgjcmtlzllyzz"
			+ "pcw jyzeckzdqyqpcjcyzmbbcydcnltrmfgyqbsygmdqqzmkql"
			+ "pgtbqcjfkjcxbljmswmdt  ldlppbxcwkcbjczhkphyyhzkzmp" + "jysylpnyyxdb";

	private static final String GBK_4 = "kxxmzjxsttdzxxbzyshjpfxpqbyljqkyzzzwl zgfwyctjxjpy"
			+ "yspmsmydyshqy zchmjmcagcfbbhplxtyqx djgxdhkxxnbhrm"
			+ "lnjsltsmrnlxqjyzlsqglbhdcgyqyyhwfjybbyjyjjdpqyapfx"
			+ "cgjscrssyz lbzjjjlgxzyxyxsqkxbxxgcxpld wetdwwcjmbt"
			+ "xchxyxxfxllj fwdpzsmylmwytcbcecblgdbqzqfjdjhymcxtx"
			+ "drmjwrh xcjzylqdyhlsrsywwzjymtllltqcjzbtckzcyqjzqa"
			+ "lmyhwwdxzxqdllqsgjfjljhjazdjgtkhsstcyjfpszlxzxrwgl"
			+ "dlzr lzqtgslllllyxxqgdzybphl x bpfd   hy jcc dmzpp"
			+ "z cyqxldozlwdwyythcqsccrsslfzfp qmbjxlmyfgjb m jwd"
			+ "n mmjtgbdzlp hsymjyl hdzjcctlcl ljcpddqdsznbgzxxcx"
			+ "qycbzxzfzfjsnttjyhtcmjxtmxspdsypzgmljtycbmdkycsz z"
			+ "yfyctgwhkyjxgyclndzscyzssdllqflqllxfdyhxggnywyllsd"
			+ "lbbjcyjzmlhl xyyytdlllb b bqjzmpclmjpgehbcqax hhhz"
			+ "chxyhjaxhlphjgpqqzgjjzzgzdqybzhhbwyffqdlzljxjpalxz"
			+ "daglgwqyxxxfmmsypfmxsyzyshdzkxsmmzzsdnzcfp ltzdnmx"
			+ "zymzmmxhhczjemxxksthwlsqlzllsjphlgzyhmxxhgzcjmhxtx"
			+ "fwkmwkdthmfzzydkmsclcmghsxpslcxyxmkxyah jzmcsnxyym"
			+ "mpmlgxmhlmlqmxtkzqyszjshyzjzybdqzwzqkdjlfmekzjpezs"
			+ "wjmzyltemznplplbpykkqzkeqlwayyplhhaq jkqclhyxxmlyc"
			+ "cyskg  lcnszkyzkcqzqljpmzhxlywqlnrydtykwszdxddntqd"
			+ "fqqmgseltthpwtxxlwydlzyzcqqpllkcc ylbqqczcljslzjxd"
			+ "dbzqdljxzqjyzqkzljcyqdypp pqykjyrpcbymxkllzllfqpyl"
			+ "llmsglcyrytmxyzfdzrysyztfmsmcl ywzgxzggsjsgkdtggzl"
			+ "ldzbzhyyzhzywxyzymsdbzyjgtsmtfxqyjssdgslnndlyzzlrx"
			+ "trznzxnqfmyzjzykbpnlypblnzz jhtzkgyzzrdznfgxskgjtt"
			+ "yllgzzbjzklplzylxyxbjfpnjzzxcdxzyxzggrs jksmzjlsjy"
			+ "wq yhqjxpjzt lsnshrnypzt wchklpszlcyysjylybbwzpdwg"
			+ "cyxckdzxsgzwwyqyytctdllxwkczkkcclgcqqdzlqcsfqchqhs"
			+ "fmqzlnbbshzdysjqplzcd cwjkjlpcmz jsqyzyhcpydsdzngq"
			+ "mbsflnffgfsm q lgqcyybkjsrjhzldcftlljgjhtxzcszztjg"
			+ "gkyoxblzppgtgyjdhz zzllqfzgqjzczbxbsxpxhyyclwdqjjx"
			+ "mfdfzhqqmqg yhtycrznqxgpdzcszcljbhbzcyzzppyzzsgyhc"
			+ "kpzjljnsc sllxb mstldfjmkdjslxlsz p pgjllydszgql l"
			+ "kyyhzttnt  tzzbsz ztlljtyyll llqyzqlbdzlslyyzyfszs"
			+ "nhnc   bbwsk rbc zm  gjmzlshtslzbl q xflyljqbzg st"
			+ "bmzjlxfnb xjztsfjmssnxlkbhsjxtnlzdntljjgzjyjczxygy"
			+ "hwrwqnztn fjszpzshzjfyrdjfcjzbfzqchzxfxsbzqlzsgyft"
			+ "zdcszxzjbqmszkjrhyjzckmjkhchgtxkjqalxbxfjtrtylxjhd"
			+ "tsjx j jjzmzlcqsbtxhqgxtxxhxftsdkfjhzxjfj  zcdlllt"
			+ "qsqzqwqxswtwgwbccgzllqzbclmqqtzhzxzxljfrmyzflxys x"
			+ "xjk xrmqdzdmmyxbsqbhgcmwfwtgmxlzpyytgzyccddyzxs g "
			+ "yjyznbgpzjcqswxcjrtfycgrhztxszzt cbfclsyxzlzqmzlmp"
			+ " lxzjxslbysmqhxxz rxsqzzzsslyflczjrcrxhhzxq dshjsj"
			+ "jhqcxjbcynsssrjbqlpxqpymlxzkyxlxcjlcycxxzzlxlll hr"
			+ "zzdxytyxcxff bpxdgygztcqwyltlswwsgzjmmgtjfsgzyafsm"
			+ "lpfcwbjcljmzlpjjlmdyyyfbygyzgyzyrqqhxy kxygy fsfsl"
			+ "nqhcfhccfxblplzyxxxkhhxshjzscxczwhhhplqalpqahxdlgg"
			+ "gdrndtpyqjjcljzljlhyhyqydhz zczywteyzxhsl jbdgwxpc"
			+ "  tjckllwkllcsstknzdnqnttlzsszyqkcgbhcrrychfpfyrwq"
			+ "pxxkdbbbqtzkznpcfxmqkcypzxehzkctcmxxmx nwwxjyhlstm"
			+ "csqdjcxctcnd p lccjlsblplqcdnndscjdpgwmrzclodansyz"
			+ "rdwjjdbcxwstszyljpxloclgpcjfzljyl c cnlckxtpzjwcyx"
			+ "wfzdknjcjlltqcbxnw xbxklylhzlqzllzxwjljjjgcmngjdzx"
			+ "txcxyxjjxsjtstp ghtxdfptffllxqpk fzflylybqjhzbmddb"
			+ "cycld tddqlyjjwqllcsjpyyclttjpycmgyxzhsztwqwrfzhjg"
			+ "azmrhcyy ptdlybyznbbxyxhzddnh msgbwfzzjcyxllrzcyxz"
			+ "lwjgcggnycpmzqzhfgtcjeaqcpjcs dczdwldfrypysccwbxgz"
			+ "mzztqscpxxjcjychcjwsnxxwjn mt mcdqdcllwnk zgglcczm"
			+ "lbqjqdsjzzghqywbzjlttdhhcchflsjyscgc zjbypbpdqkxwy"
			+ "yflxncwcxbmaykkjwzzzrxy yqjfljphhhytzqmhsgzqwbwjdy"
			+ "sqzxslzyymyszg x hysyscsyznlqyljxcxtlwdqzpcycyppnx"
			+ "fyrcmsmslxglgctlxzgz g tc dsllyxmtzalcpxjtjwtcyyjb"
			+ "lbzlqmylxpghdlssdhbdcsxhamlzpjmcnhjysygchskqmc lwj"
			+ "xsmocdrlyqzhjmyby lyetfjfrfksyxftwdsxxlysjslyxsnxy"
			+ "yxhahhjzxwmljcsqlkydztzsxfdxgzjksxybdpwnzwpczczeny"
			+ "cxqfjykbdmljqq lxslyxxylljdzbsmhpsttqqwlhogyblzzal"
			+ "xqlzerrqlstmypyxjjxqsjpbryxyjlxyqylthylymlkljt llh"
			+ "fzwkhljlhlj klj tlqxylmbtxchxcfxlhhhjbyzzkbxsdqc j"
			+ "zsyhzxfebcqwyyjqtzyqhqqzmwffhfrbntpcjlfzgppxdbbztg"
			+ " gchmfly xlxpqsywmngqlxjqjtcbhxspxlbyyjddhsjqyjxll"
			+ "dtkhhbfwdysqrnwldebzwcydljtmxmjsxyrwfymwrxxysztzzt"
			+ "ymldq xlyq jtscxwlprjwxhyphydnxhgmywytzcs tsdlwdcq"
			+ "pyclqyjwxwzzmylclmxcmzsqtzpjqblgxjzfljjytjnxmcxs c"
			+ "dl dyjdqcxsqyclzxzzxmxqrjhzjphfljlmlqnldxzlllfypny"
			+ "ysxcqqcmjzzhnpzmekmxkyqlxstxxhwdcwdzgyyfpjzdyzjzx "
			+ "rzjchrtlpyzbsjhxzypbdfgzzrytngxcqy b cckrjjbjerzgy"
			+ "  xknsjkljsjzljybzsqlbcktylccclpfyadzyqgk tsfc xdk"
			+ "dyxyfttyh  wtghrynjsbsnyjhkllslydxxwbcjsbbpjzjcjdz"
			+ "bfxxbrjlaygcsndcdszblpz dwsbxbcllxxlzdjzsjy lyxfff"
			+ "bhjjxgbygjpmmmpssdzjmtlyzjxswxtyledqpjmygqzjgdblqj"
			+ "wjqllsdgytqjczcjdzxqgsgjhqxnqlzbxsgzhcxy ljxyxydfq"
			+ "qjjfxdhctxjyrxysqtjxyebyyssyxjxncyzxfxmsyszxy schs"
			+ "hxzzzgzcgfjdltynpzgyjyztyqzpbxcbdztzc zyxxyhhsqxsh"
			+ "dhgqhjhgxwsztmmlhyxgcbtclzkkwjzrclekxtdbcykqqsayxc"
			+ "jxwwgsbhjyzs  csjkqcxswxfltynytpzc czjqtzwjqdzzzqz"
			+ "ljjxlsbhpyxxpsxshheztxfptjqyzzxhyaxncfzyyhxgnxmywx"
			+ "tcspdhhgymxmxqcxtsbcqsjyxxtyyly pclmmszmjzzllcogxz"
			+ "aajzyhjmzxhdxzsxzdzxleyjjzjbhzmzzzqtzpsxztdsxjjlny"
			+ "azhhyysrnqdthzhayjyjhdzjzlsw cltbzyecwcycrylcxnhzy"
			+ "dzydtrxxbzsxqhxjhhlxxlhdlqfdbsxfzzyychtyyjbhecjkgj"
			+ "fxhzjfxhwhdzfyapnpgnymshk mamnbyjtmxyjcthjbzyfcgty"
			+ "hwphftwzzezsbzegpbmtskftycmhbllhgpzjxzjgzjyxzsbbqs"
			+ "czzlzccstpgxmjsftcczjz djxcybzlfcjsyzfgszlybcwzzby"
			+ "zdzypswyjgxzbdsysxlgzybzfyxxxccxtzlsqyxzjqdcztdxzj"
			+ "jqcgxtdgscxzsyjjqcc ldqztqchqqjzyezwkjcfypqtynlmkc"
			+ "qzqzbqnyjddzqzxdpzjcdjstcjnxbcmsjqmjqwwjqnjnlllwqc"
			+ "qqdzpzydcydzcttf znztqzdtjlzbclltdsxkjzqdpzlzntjxz"
			+ "bcjltqjldgdbbjqdcjwynzyzcdwllxwlrxntqqczxkjld tdgl"
			+ " lajjkly kqll dz td ycggjyxdxfrskstqdenqmrkq  hgkd"
			+ "ldazfkypbggpzrebzzykyqspegjjglkqzzzslysywqzwfqzylz"
			+ "zlzhwcgkyp qgnpgblplrrjyxcccyyhsbzfybnyytgzxylxczw"
			+ "h zjzblfflgskhyjzeyjhlplllldzlyczblcybbxbcbpnnzc r"
			+ " sycgyy qzwtzdxtedcnzzzty hdynyjlxdjyqdjszwlsh lbc"
			+ "zpyzjyctdyntsyctszyyegdw ycxtscysmgzsccsdslccrqxyy"
			+ "elsm xztebblyylltqsyrxfkbxsychbjbwkgskhhjh xgnlycd"
			+ "lfyljgbxqxqqzzplnypxjyqymrbsyyhkxxstmxrczzywxyhymc"
			+ "l lzhqwqxdbxbzwzmldmyskfmklzcyqyczqxzlyyzmddz ftqp"
			+ "czcyypzhzllytztzxdtqcy ksccyyazjpcylzyjtfnyyynrs y"
			+ "lmmnxjsmyb sljqyldzdpqbzzblfndsqkczfywhgqmrdsxycyt"
			+ "xnq jpyjbfcjdyzfbrxejdgyqbsrmnfyyqpghyjdyzxgr htk "
			+ "leq zntsmpklbsgbpyszbydjzsstjzytxzphsszsbzczptqfzm"
			+ "yflypybbjgxzmxxdjmtsyskkbzxhjcelbsmjyjzcxt mljshrz"
			+ "zslxjqpyzxmkygxxjcljprmyygadyskqs dhrzkqxzyztcghyt"
			+ "lmljxybsyctbhjhjfcwzsxwwtkzlxqshlyjzjxe mplprcglt "
			+ "zztlnjcyjgdtclklpllqpjmzbapxyzlkktgdwczzbnzdtdyqzj"
			+ "yjgmctxltgcszlmlhbglk  njhdxphlfmkyd lgxdtwzfrjejz"
			+ "tzhydxykshwfzcqshknqqhtzhxmjdjskhxzjzbzzxympagjmst"
			+ "bxlskyynwrtsqlscbpspsgzwyhtlksssw hzzlyytnxjgmjszs"
			+ "xfwnlsoztxgxlsmmlbwldszylkqcqctmycfjbslxclzzclxxks"
			+ "bjqclhjpsqplsxxckslnhpsfqqytxy jzlqldtzqjzdyydjnzp"
			+ "d cdskjfsljhylzsqzlbtxxdgtqbdyazxdzhzjnhhqbyknxjjq"
			+ "czmlljzkspldsclbblzkleljlbq ycxjxgcnlcqplzlznjtzlx"
			+ "yxpxmyzxwyczyhzbtrblxlcczjadjlmmmsssmybhb kkbhrsxx"
			+ "jmxsdynzpelbbrhwghfchgm  klltsjyycqltskywyyhywxbxq"
			+ "ywbawykqldq tmtkhqcgdqktgpkxhcpthtwthkshthlxyzyyda"
			+ "spkyzpceqdltbdssegyjq xcwxssbz dfydlyjcls yzyexcyy"
			+ "sdwnzajgyhywtjdaxysrltdpsyxfnejdy lxllqzyqqhgjhzyc"
			+ "shwshczyjxllnxzjjn fxmfpycyawddhdmczlqzhzyztldywll"
			+ "hymmylmbwwkxydtyldjpyw xjwmllsafdllyflb   bqtzcqlj"
			+ "tfmbthydcqrddwr qnysnmzbyytbjhp ygtjahg tbstxkbtzb"
			+ "kldbeqqhqmjdyttxpgbktlgqxjjjcthxqdwjlwrfwqgwqhckry"
			+ "swgftgygbxsd wdfjxxxjzlpyyypayxhydqkxsaxyxgskqhykf"
			+ "dddpplcjlhqeewxksyykdbplfjtpkjltcyyhhjttpltzzcdlsh"
			+ "qkzjqyste eywyyzy xyysttjkllpwmcyhqgxyhcrmbxpllnqt"
			+ "jhyylfd fxzpsftljxxjbswyysksflxlpplbbblbsfxyzsylff"
			+ "fscjds tztryysyffsyzszbjtbctsbsdhrtjjbytcxyje xbne"
			+ "bjdsysykgsjzbxbytfzwgenhhhhzhhtfwgzstbgxklsty mtmb"
			+ "yxj skzscdyjrcwxzfhmymcxlzndtdh xdjggybfbnbpthfjaa"
			+ "xwfpxmyphdttcxzzpxrsywzdlybbjd qwqjpzypzjznjpzjlzt"
			+ " fysbttslmptzrtdxqsjehbzyj dhljsqmlhtxtjecxslzzspk"
			+ "tlzkqqyfs gywpcpqfhqhytqxzkrsg gsjczlptxcdyyzss qz"
			+ "slxlzmycbcqbzyxhbsxlzdltcdjtylzjyyzpzylltxjsjxhlbr"
			+ "ypxqzskswwwygyabbztqktgpyspxbjcmllxztbklgqkq lsktf"
			+ "xrdkbfpftbbrfeeqgypzsstlbtpszzsjdhlqlzpmsmmsxlqqnk"
			+ "nbrddnxxdhddjyyyfqgzlxsmjqgxytqlgpbqxcyzy drj gtdj"
			+ "yhqshtmjsbwplwhlzffny  gxqhpltbqpfbcwqdbygpnztbfzj"
			+ "gsdctjshxeawzzylltyybwjkxxghlfk djtmsz sqynzggswqs"
			+ "phtlsskmcl  yszqqxncjdqgzdlfnykljcjllzlmzjn   scht"
			+ "hxzlzjbbhqzwwycrdhlyqqjbeyfsjxwhsr  wjhwpslmssgztt"
			+ "ygyqqwr lalhmjtqjcmxqbjjzjxtyzkxbyqxbjxshzssfjlxmx"
			+ "  fghkzszggylcls rjyhslllmzxelgl xdjtbgyzbpktzhkzj"
			+ "yqsbctwwqjpqwxhgzgdyfljbyfdjf hsfmbyzhqgfwqsyfyjgp"
			+ "hzbyyzffwodjrlmftwlbzgycqxcdj ygzyyyyhy xdwegazyhx"
			+ "jlzythlrmgrxxzcl   ljjtjtbwjybjjbxjjtjteekhwslj lp"
			+ "sfyzpqqbdlqjjtyyqlyzkdksqj yyqzldqtgjj  js cmraqth"
			+ "tejmfctyhypkmhycwj cfhyyxwshctxrljhjshccyyyjltktty"
			+ "tmxgtcjtzaxyoczlylbszyw jytsjyhbyshfjlygjxxtmzyylt"
			+ "xxypzlxyjzyzyybnhmymdyylblhlsyygqllscxlxhdwkqgyshq"
			+ "ywljyyhzmsljljxcjjyy cbcpzjmylcqlnjqjlxyjmlzjqlycm"
			+ "hcfmmfpqqmfxlmcfqmm znfhjgtthkhchydxtmqzymyytyyyzz"
			+ "dcymzydlfmycqzwzz mabtbcmzzgdfycgcytt fwfdtzqssstx"
			+ "jhxytsxlywwkxexwznnqzjzjjccchyyxbzxzcyjtllcqxynjyc"
			+ "yycynzzqyyyewy czdcjyhympwpymlgkdldqqbchjxy       "
			+ "                                                  " + "                 sypszsjczc     cqytsjljjt   ";

	public static String getGBKpy(String hzString) throws UnsupportedEncodingException {
		/*
		 * 效率:处理大字符串(字符串有132055个byte,即70577个char)1000次，消耗时间44.474S.
		 */
		if (hzString == null || hzString.length() == 0)
			return "";
		int pyi, len, no;
		int ch1code = 0, ch2code = 0;
		char ch1, ch2;

		StringBuffer pyBuffer = new StringBuffer();
		byte eB[] = hzString.getBytes("GBK");
		len = eB.length;

		// 开始计算
		pyi = 0;
		while (pyi < len) {
			ch1 = (char) eB[pyi];
			pyi = pyi + 1;
			ch1code = ch1;
			if (ch1code > 0 && ch1code < 129) {
				// 普通的acsii
				pyBuffer.append(ch1);
				continue;
			} else {
				// GBK字符
				ch1 = (char) (256 + (int) ch1);
				if (eB[pyi] < 0) {
					ch2 = (char) (256 + (int) eB[pyi]);
				} else {
					ch2 = (char) eB[pyi];
				}
				pyi = pyi + 1;
				if (pyi > len)
					break;
			}
			ch1code = ch1;
			ch2code = ch2;
			if (ch1code <= 254 && ch1code >= 170) {
				// 优先处理GB-2312汉字.
				if (ch2code > 160) {
					// 查找GB-2312
					no = (ch1code - 176) * 94 + (ch2code - 160);
					pyBuffer.append(GB_2312.charAt(no - 1));
				} else {
					// 查找GBK_4
					no = (ch1code - 170) * 97 + (ch2code - 63);
					pyBuffer.append(GBK_4.charAt(no - 1));
				}
			} else if (ch1code <= 160 && ch1code >= 129) {
				// 查找GBK_3
				no = (ch1code - 129) * 191 + (ch2code - 63);
				pyBuffer.append(GBK_3.charAt(no - 1));
			} else {
				// 不是GBK汉字
				continue;
			}
		}
		return pyBuffer.toString().trim().toLowerCase();
	}

	// 字符串转成时间
	public static Date getDate(String dt) throws ParseException {
		SimpleDateFormat sdf1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
		System.out.println();
		return sdf1.parse(dt);
	}

	
	/**
	 * @获取UUID
	 * @author Fangk
	 */
	public static String getUUID() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	public static void main(String[] args) throws UnsupportedEncodingException, ParseException {
		System.out.println(getDistanceTime(Tool.getDateString(new Date()), "2017-07-10 15:07:05"));
	}

}
