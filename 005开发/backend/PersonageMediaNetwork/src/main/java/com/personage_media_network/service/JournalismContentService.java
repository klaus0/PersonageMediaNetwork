package com.personage_media_network.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.JournalismContentMapper;
import com.personage_media_network.dao.JournalismMapper;
import com.personage_media_network.entity.Journalism;
import com.personage_media_network.entity.JournalismContent;
import com.personage_media_network.enums.ResultEnum;
import com.personage_media_network.exception.CustomException;
import com.personage_media_network.httpbean.req.JournalismContentReq;
import com.personage_media_network.httpbean.res.JournalismContentRes;

/**
 * 咨讯详情
 * 
 * @author klaus
 *
 */
@Service
public class JournalismContentService {
	@Autowired
	private JournalismContentMapper journalismContentMapper;
	@Autowired
	private JournalismMapper journalismMapper;

	/**
	 * 查询
	 * 
	 * @return
	 */
	public ReturnCode getJournalismContent(String journalismId) {

		JournalismContentRes journalismContent = journalismContentMapper.getJournalismContent(journalismId);
		// 如果存在 便返回第一条
		if (journalismContent != null) {

			return new Builder().code(0).msg("查询成功").object(journalismContent).build();
		} else {
			// 未查到信息
			throw new CustomException(ResultEnum.NO_INFORMATION);
		}
	}

	// 新增

	@Transactional
	public ReturnCode saveJournalismContent(JournalismContentReq journalismContentReq) {

		Journalism journalism = journalismContentReq.getJournalism();

		JournalismContent journalismContent = journalismContentReq.getJournalismContent();

		journalismMapper.insertSelective(journalism);
		
		journalismContentMapper.insertSelective(journalismContent);

		return new Builder().code(0).msg("保存成功").object(journalismContentReq).build();

	}
	
	
	//修改
	@Transactional
	public ReturnCode updateOrRemoveJournalismContent(JournalismContentReq journalismContentReq) {

		Journalism journalism = journalismContentReq.getJournalism();

		JournalismContent journalismContent = journalismContentReq.getJournalismContent();

		journalismMapper.updateByPrimaryKeySelective(journalism);
		
		journalismContentMapper.updateByPrimaryKeySelective(journalismContent);

		return new Builder().code(0).msg("保存成功").object(journalismContentReq).build();

	}

}
