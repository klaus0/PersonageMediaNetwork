package com.personage_media_network.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.AboutUsMapper;
import com.personage_media_network.entity.AboutUs;
import com.personage_media_network.enums.ResultEnum;
import com.personage_media_network.exception.CustomException;

/**
 * 关于我们
 * 
 * @author klaus
 *
 */
@Service
public class AboutUsService {

	@Autowired
	private AboutUsMapper aboutUsMapper;

	/**
	 * 查询
	 * 
	 * @return
	 */
	public ReturnCode getAboutUsInfo() {

		List<AboutUs> aboutUses = aboutUsMapper.selectAboutAsInfo();
		// 如果存在 便返回第一条
		if (aboutUses != null && aboutUses.size() > 0) {

			return new Builder().code(0).msg("查询成功").object(aboutUses.get(0)).build();
		} else {
			// 未查到信息
			throw new CustomException(ResultEnum.NO_INFORMATION);
		}

	}

	/**
	 * 修改
	 * 
	 * @param aboutUs
	 * @return
	 */
	@Transactional
	public ReturnCode updateAboutUsInfo(AboutUs aboutUs) {

		aboutUsMapper.updateByPrimaryKeySelective(aboutUs);

		return new Builder().code(0).msg("更新成功").build();

	}

}
