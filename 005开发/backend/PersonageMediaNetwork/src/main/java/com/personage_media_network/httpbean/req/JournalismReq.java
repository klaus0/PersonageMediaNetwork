package com.personage_media_network.httpbean.req;

import com.personage_media_network.entity.Journalism;
import com.personage_media_network.entity.Paging;

public class JournalismReq extends Journalism {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Paging paging;

	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

}
