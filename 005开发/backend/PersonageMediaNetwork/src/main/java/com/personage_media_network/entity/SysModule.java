package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;
/**
 * 菜单表
 * @author klaus
 *
 */
public class SysModule implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String moduleId;//菜单ID

    private String moduleName;//菜单名称

    private String modulePid;//菜单父ID

    private String moduleUrl;//菜单路径

    private Integer moduleSort;//菜单序号

    private String moduleInfo;//菜单描述

    private Boolean isDelete;//是否删除

    private Boolean isEnable;//是否可用

    private Date createTime;//创建时间

    private Date updateTime;//更新时间

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId == null ? null : moduleId.trim();
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName == null ? null : moduleName.trim();
    }

    public String getModulePid() {
        return modulePid;
    }

    public void setModulePid(String modulePid) {
        this.modulePid = modulePid == null ? null : modulePid.trim();
    }

    public String getModuleUrl() {
        return moduleUrl;
    }

    public void setModuleUrl(String moduleUrl) {
        this.moduleUrl = moduleUrl == null ? null : moduleUrl.trim();
    }

    public Integer getModuleSort() {
        return moduleSort;
    }

    public void setModuleSort(Integer moduleSort) {
        this.moduleSort = moduleSort;
    }

    public String getModuleInfo() {
        return moduleInfo;
    }

    public void setModuleInfo(String moduleInfo) {
        this.moduleInfo = moduleInfo == null ? null : moduleInfo.trim();
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}