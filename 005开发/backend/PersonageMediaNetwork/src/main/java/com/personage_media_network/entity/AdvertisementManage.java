package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 广告投放管理
 * 
 * @author klaus
 *
 */
public class AdvertisementManage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String advertisement_manage;// 广告投放管理ID

	@NotBlank(message = "广告投放类型不能为空")
	private String advertiserType;// 广告投放类型
	@NotBlank(message = "广告人姓名")
	private String advertiserName;// 广告人姓名
	@Email(message = "请检查邮箱格式")
	private String advertiserEmail;// 广告人邮箱
	@Pattern(regexp = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$", message = "请检查手机格式")
	private String advertiserPhone;// 广告人手机

	private String advertiserWechat;// 广告人微信
	@NotBlank(message = "公司必填")
	private String advertiserCompany;// 广告人公司

	private String advertiserMessage;// 广告人留言

	private Date createtTime;// 创建时间

	private Date updateTime;// 更新时间

	private Boolean isDelete;// 是否删除

	public String getAdvertisement_manage() {
		return advertisement_manage;
	}

	public void setAdvertisement_manage(String advertisement_manage) {
		this.advertisement_manage = advertisement_manage == null ? null : advertisement_manage.trim();
	}

	public String getAdvertiserType() {
		return advertiserType;
	}

	public void setAdvertiserType(String advertiserType) {
		this.advertiserType = advertiserType == null ? null : advertiserType.trim();
	}

	public String getAdvertiserName() {
		return advertiserName;
	}

	public void setAdvertiserName(String advertiserName) {
		this.advertiserName = advertiserName == null ? null : advertiserName.trim();
	}

	public String getAdvertiserEmail() {
		return advertiserEmail;
	}

	public void setAdvertiserEmail(String advertiserEmail) {
		this.advertiserEmail = advertiserEmail == null ? null : advertiserEmail.trim();
	}

	public String getAdvertiserPhone() {
		return advertiserPhone;
	}

	public void setAdvertiserPhone(String advertiserPhone) {
		this.advertiserPhone = advertiserPhone == null ? null : advertiserPhone.trim();
	}

	public String getAdvertiserWechat() {
		return advertiserWechat;
	}

	public void setAdvertiserWechat(String advertiserWechat) {
		this.advertiserWechat = advertiserWechat == null ? null : advertiserWechat.trim();
	}

	public String getAdvertiserCompany() {
		return advertiserCompany;
	}

	public void setAdvertiserCompany(String advertiserCompany) {
		this.advertiserCompany = advertiserCompany == null ? null : advertiserCompany.trim();
	}

	public String getAdvertiserMessage() {
		return advertiserMessage;
	}

	public void setAdvertiserMessage(String advertiserMessage) {
		this.advertiserMessage = advertiserMessage == null ? null : advertiserMessage.trim();
	}

	public Date getCreatetTime() {
		return createtTime;
	}

	public void setCreatetTime(Date createtTime) {
		this.createtTime = createtTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
}