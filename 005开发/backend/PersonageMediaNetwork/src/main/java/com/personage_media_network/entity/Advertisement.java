package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * 广告
 * 
 * @author klaus
 *
 */
public class Advertisement implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String advertisementId;// 广告ID
	@NotBlank(message = "图片不能为空")
	private String advertisementImage;// 广告图片
	@NotBlank(message = "跳转路径不能为空")
	private String advertisementUrl;// 广告跳转路径
	@NotBlank(message = "是否橱窗不能为空")
	private Boolean isShowcase;// 是否橱窗
	@NotBlank(message = "主次不能为空")
	private String primaryAndSecondary;// 主次

	private Date createTime;// 创建时间

	private Date updateTime;// 更新时间

	private Boolean isDelete;// 是否删除

	private Boolean isEnable;// 是否可用

	public String getAdvertisementId() {
		return advertisementId;
	}

	public void setAdvertisementId(String advertisementId) {
		this.advertisementId = advertisementId == null ? null : advertisementId.trim();
	}

	public String getAdvertisementImage() {
		return advertisementImage;
	}

	public void setAdvertisementImage(String advertisementImage) {
		this.advertisementImage = advertisementImage == null ? null : advertisementImage.trim();
	}

	public String getAdvertisementUrl() {
		return advertisementUrl;
	}

	public void setAdvertisementUrl(String advertisementUrl) {
		this.advertisementUrl = advertisementUrl == null ? null : advertisementUrl.trim();
	}

	public Boolean getIsShowcase() {
		return isShowcase;
	}

	public void setIsShowcase(Boolean isShowcase) {
		this.isShowcase = isShowcase;
	}

	public String getPrimaryAndSecondary() {
		return primaryAndSecondary;
	}

	public void setPrimaryAndSecondary(String primaryAndSecondary) {
		this.primaryAndSecondary = primaryAndSecondary == null ? null : primaryAndSecondary.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Boolean getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(Boolean isEnable) {
		this.isEnable = isEnable;
	}
}