package com.personage_media_network.dao;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.SysRoldModule;
@Mapper
public interface SysRoldModuleMapper {
    int deleteByPrimaryKey(String id);

    int insert(SysRoldModule record);

    int insertSelective(SysRoldModule record);

    SysRoldModule selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysRoldModule record);

    int updateByPrimaryKey(SysRoldModule record);
}