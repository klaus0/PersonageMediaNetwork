package com.personage_media_network.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.SubmissionMapper;
import com.personage_media_network.entity.PageBean;
import com.personage_media_network.entity.Submission;

/**
 * 投稿与转载
 * 
 * @author klaus
 *
 */
@Service
public class SubmissionService {
	@Autowired
	private SubmissionMapper submissionMapper;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	public ReturnCode listSubmission(Submission submission) {

		PageBean<Submission> page = new PageBean<Submission>();
		page.setItemTotal(submissionMapper.selectSubmissionListCount(submission));
		page.setItems(submissionMapper.selectSubmissionList(submission));
		return new Builder().code(0).msg("查询成功").object(page).build();
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode saveSubmission(Submission submission) {

		submissionMapper.insertSelective(submission);

		return new Builder().code(0).msg("保存成功").object(submission).build();

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode updateOrRemoveSubmission(Submission submission) {

		submissionMapper.updateByPrimaryKeySelective(submission);

		return new Builder().code(0).msg("操作成功").object(submission).build();

	}
}
