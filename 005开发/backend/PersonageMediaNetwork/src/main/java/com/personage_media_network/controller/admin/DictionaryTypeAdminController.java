package com.personage_media_network.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.entity.DictionaryType;
import com.personage_media_network.service.DictionaryTypeService;

import com.personage_media_network.ReturnCode.Builder;

@RestController
@RequestMapping("admin/dictionaryType")
public class DictionaryTypeAdminController {
	@Autowired
	private DictionaryTypeService dictionaryTypeService;

	/**
	 * 全查字典类型
	 * 
	 * @return
	 */
	@GetMapping("listDictionaryType")
	public ReturnCode listDictionaryType() {

		return dictionaryTypeService.listDictionaryType();
	}

	/**
	 * 保存
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@PostMapping("saveDictionaryType")
	public ReturnCode saveDictionaryType(@RequestBody @Valid DictionaryType dictionaryType,
			BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage()).object(dictionaryType)
					.build();
		}

		return dictionaryTypeService.saveDictionaryType(dictionaryType);
	}

	/**
	 * 修改
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@PutMapping("updateDictionaryType")
	public ReturnCode updateDictionaryType(@RequestBody @Valid DictionaryType dictionaryType,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage()).object(dictionaryType)
					.build();
		}

		return dictionaryTypeService.updateDictionaryType(dictionaryType);
	}

	/**
	 * 根据字典类型Pym查询字典类型
	 * 
	 * @param dictionaryTypePym
	 * @return
	 */
	@GetMapping("getDictionaryTypeByDictionaryTypePym")
	public ReturnCode getDictionaryTypeByDictionaryTypePym(String dictionaryTypePym) {

		return dictionaryTypeService.getDictionaryTypeByDictionaryTypePym(dictionaryTypePym);
	}
}
