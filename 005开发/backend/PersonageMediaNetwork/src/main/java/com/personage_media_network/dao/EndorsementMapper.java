package com.personage_media_network.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.Endorsement;
import com.personage_media_network.httpbean.req.EndorsementReq;
@Mapper
public interface EndorsementMapper {
    int deleteByPrimaryKey(String endorsementId);

    int insert(Endorsement record);

    int insertSelective(Endorsement record);

    Endorsement selectByPrimaryKey(String endorsementId);

    int updateByPrimaryKeySelective(Endorsement record);

    int updateByPrimaryKey(Endorsement record);

	int selectEndorsementListCount(EndorsementReq endorsementReq);

	List<Endorsement> selectEndorsementtionList(EndorsementReq endorsementReq);
}