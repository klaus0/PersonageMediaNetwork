package com.personage_media_network.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.Interview;
@Mapper
public interface InterviewMapper {
    int deleteByPrimaryKey(String interviewId);

    int insert(Interview record);

    int insertSelective(Interview record);

    Interview selectByPrimaryKey(String interviewId);

    int updateByPrimaryKeySelective(Interview record);

    int updateByPrimaryKey(Interview record);

	int selectInterviewListCount(Interview interview);

	List<Interview> selectInterviewList(Interview interview);
}