package com.personage_media_network.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.DictionaryTypeMapper;
import com.personage_media_network.entity.DictionaryType;
import com.personage_media_network.enums.ResultEnum;
import com.personage_media_network.exception.CustomException;

/**
 * 字典类型
 * 
 * @author klaus
 *
 */
@Service
public class DictionaryTypeService {

	@Autowired
	private DictionaryTypeMapper dictionaryTypeMapper;

	/**
	 * 全查字典类型
	 * 
	 * @return
	 */
	public ReturnCode listDictionaryType() {

		List<DictionaryType> dictionaryTypeList = dictionaryTypeMapper.listDetectionType();

		return new Builder().code(0).msg("查询成功").object(dictionaryTypeList).build();
	}

	/**
	 * 保存
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@Transactional
	public ReturnCode saveDictionaryType(DictionaryType dictionaryType) {

		// 防止pym重复
		int rePym = dictionaryTypeMapper.selectCheckDictionaryTypePymRepeat(dictionaryType.getDictionaryTypePym());
		if (rePym > 0) {

			throw new CustomException(ResultEnum.PYM_REPEAT);

		}

		// 防止名称重复
		int reName = dictionaryTypeMapper.selectCheckDictionaryTypeNameRepeat(dictionaryType.getDictionaryTypeName());

		if (reName > 0) {

			throw new CustomException(ResultEnum.NAME_REPEAT);

		}

		// 添加序号
		int reSort = dictionaryTypeMapper.selectMaxDictionaryTypeSort();
		if (reSort == 0) {
			reSort = 1;
		}
		dictionaryType.setDictionaryTypeSort(reSort);

		dictionaryTypeMapper.insertSelective(dictionaryType);

		return new Builder().code(0).msg("保存成功").object(dictionaryType).build();
	}

	/**
	 * 修改
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@Transactional
	public ReturnCode updateDictionaryType(DictionaryType dictionaryType) {

		// 防止pym重复
		int rePym = dictionaryTypeMapper.selectCheckDictionaryTypePymRepeat(dictionaryType.getDictionaryTypePym());
		if (rePym > 0) {

			throw new CustomException(ResultEnum.PYM_REPEAT);

		}

		// 防止名称重复
		int reName = dictionaryTypeMapper.selectCheckDictionaryTypeNameRepeat(dictionaryType.getDictionaryTypeName());

		if (reName > 0) {

			throw new CustomException(ResultEnum.NAME_REPEAT);

		}

		dictionaryTypeMapper.updateByPrimaryKeySelective(dictionaryType);

		return new Builder().code(0).msg("更新成功").object(dictionaryType).build();
	}

	/**
	 * 根据字典类型Pym查询字典类型
	 * 
	 * @param dictionaryTypePym
	 * @return
	 */
	public ReturnCode getDictionaryTypeByDictionaryTypePym(String dictionaryTypePym) {

		DictionaryType dictionaryType = dictionaryTypeMapper.selectDictionaryTypeByDictionaryTypePym(dictionaryTypePym);

		return new Builder().code(0).msg("获取成功").object(dictionaryType).build();
	}

}
