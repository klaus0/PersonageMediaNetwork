package com.personage_media_network.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.DictionaryItemsMapper;
import com.personage_media_network.entity.DictionaryItems;
import com.personage_media_network.enums.ResultEnum;
import com.personage_media_network.exception.CustomException;

/**
 * 字典内容
 * @author klaus
 *
 */
@Service
public class DictionaryItemsService {
	@Autowired
	private DictionaryItemsMapper dictionaryItemsMapper;

	/**
	 * 全查字典类型
	 * 
	 * @return
	 */
	public ReturnCode listDictionaryItems(String dictionaryTypeId) {

		List<DictionaryItems> dictionaryTypeList = dictionaryItemsMapper.listDictionaryItems(dictionaryTypeId);

		return new Builder().code(0).msg("查询成功").object(dictionaryTypeList).build();
	}

	/**
	 * 保存
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@Transactional
	public ReturnCode saveDictionaryItems(DictionaryItems dictionaryItems) {

		// 防止pym重复
		int rePym = dictionaryItemsMapper.selectCheckDictionaryItemPymRepeat(dictionaryItems);
		if (rePym > 0) {

			throw new CustomException(ResultEnum.PYM_REPEAT);

		}

		// 防止名称重复
		int reName = dictionaryItemsMapper.selectCheckDictionaryItemNameRepeat(dictionaryItems);

		if (reName > 0) {

			throw new CustomException(ResultEnum.NAME_REPEAT);

		}

		// 添加序号
		int reSort = dictionaryItemsMapper.selectMaxDictionaryItemSort(dictionaryItems.getDictionaryTypeId());
	
		dictionaryItems.setDictionaryItemSort(reSort+1);;

		dictionaryItemsMapper.insertSelective(dictionaryItems);

		return new Builder().code(0).msg("保存成功").object(dictionaryItems).build();
	}

	/**
	 * 修改
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@Transactional
	public ReturnCode updateDictionaryItems(DictionaryItems dictionaryItems) {

		// 防止pym重复
				int rePym = dictionaryItemsMapper.selectCheckDictionaryItemPymRepeat(dictionaryItems);
				if (rePym > 0) {

					throw new CustomException(ResultEnum.PYM_REPEAT);

				}

				// 防止名称重复
				int reName = dictionaryItemsMapper.selectCheckDictionaryItemNameRepeat(dictionaryItems);

				if (reName > 0) {

					throw new CustomException(ResultEnum.NAME_REPEAT);

				}

			

				dictionaryItemsMapper.updateByPrimaryKeySelective(dictionaryItems);

				return new Builder().code(0).msg("更新成功").object(dictionaryItems).build();
	}

	/**
	 * 根据字典类型Pym查询字典类型
	 * 
	 * @param dictionaryTypePym
	 * @return
	 */
	public ReturnCode ListDictionaryItemsByDictionaryTypePym(String dictionaryTypePym) {

		List<DictionaryItems> dictionaryItems = dictionaryItemsMapper.selectDictionaryItemByDictionaryPym(dictionaryTypePym);

		return new Builder().code(0).msg("获取成功").object(dictionaryItems).build();
	}
}
