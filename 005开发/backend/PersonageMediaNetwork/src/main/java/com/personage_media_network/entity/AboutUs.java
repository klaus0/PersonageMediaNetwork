package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * 关于我们
 * 
 * @author klaus
 *
 */
public class AboutUs implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String aboutUsId;// 关于我们ID

	@NotBlank(message = "内容不能为空")
	private String aboutUsContent;// 关于我们内容

	private Date createTime;// 更新时间

	private Date updateTime;// 创建时间

	public String getAboutUsId() {
		return aboutUsId;
	}

	public void setAboutUsId(String aboutUsId) {
		this.aboutUsId = aboutUsId == null ? null : aboutUsId.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getAboutUsContent() {
		return aboutUsContent;
	}

	public void setAboutUsContent(String aboutUsContent) {
		this.aboutUsContent = aboutUsContent == null ? null : aboutUsContent.trim();
	}
}