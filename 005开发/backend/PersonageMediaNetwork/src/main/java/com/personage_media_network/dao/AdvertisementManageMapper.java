package com.personage_media_network.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.AdvertisementManage;
import com.personage_media_network.httpbean.req.AdvertisementManageReq;

@Mapper
public interface AdvertisementManageMapper {
	int deleteByPrimaryKey(String advertisement_manage);

	int insert(AdvertisementManage record);

	int insertSelective(AdvertisementManage record);

	AdvertisementManage selectByPrimaryKey(String advertisement_manage);

	int updateByPrimaryKeySelective(AdvertisementManage record);

	int updateByPrimaryKey(AdvertisementManage record);

	/**
	 * 广告管理分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	List<AdvertisementManage> selectAdvertisementManageList(AdvertisementManageReq advertisementManageReq);

	/**
	 * 广告管理分页Count
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	int selectAdvertisementManageListCount(AdvertisementManageReq advertisementManageReq);
}