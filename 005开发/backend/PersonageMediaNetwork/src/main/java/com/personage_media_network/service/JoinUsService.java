package com.personage_media_network.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.JoinUsMapper;
import com.personage_media_network.entity.JoinUs;
import com.personage_media_network.enums.ResultEnum;
import com.personage_media_network.exception.CustomException;

/**
 * 加入我们
 * @author klaus
 *
 */
@Service
public class JoinUsService {

	
	@Autowired
	private JoinUsMapper joinUsMapper;

	/**
	 * 查询
	 * 
	 * @return
	 */
	public ReturnCode getJoinUsInfo() {

		List<JoinUs> joinUs = joinUsMapper.selectJoinUsInfo();
		// 如果存在 便返回第一条
		if (joinUs != null && joinUs.size() > 0) {

			return new Builder().code(0).msg("查询成功").object(joinUs.get(0)).build();
		} else {
			// 未查到信息
			throw new CustomException(ResultEnum.NO_INFORMATION);
		}

	}

	/**
	 * 修改
	 * 
	 * @param aboutUs
	 * @return
	 */
	@Transactional
	public ReturnCode updateJoinUsInfo(JoinUs joinUs) {

		joinUsMapper.updateByPrimaryKeySelective(joinUs);

		return new Builder().code(0).msg("更新成功").object(joinUs).build();

	}
	
}
