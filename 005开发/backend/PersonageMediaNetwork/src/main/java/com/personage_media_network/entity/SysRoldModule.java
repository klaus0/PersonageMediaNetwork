package com.personage_media_network.entity;

import java.io.Serializable;

/**
 * 角色菜单关联表
 * @author klaus
 *
 */
public class SysRoldModule implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;//角色菜单关联ID

    private String roleId;//角色ID

    private String moduleId;//菜单ID

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId == null ? null : moduleId.trim();
    }
}