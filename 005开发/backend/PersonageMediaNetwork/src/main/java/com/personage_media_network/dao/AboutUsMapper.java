package com.personage_media_network.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.AboutUs;

@Mapper
public interface AboutUsMapper {
    int deleteByPrimaryKey(String aboutUsId);

    int insert(AboutUs record);

    int insertSelective(AboutUs record);

    AboutUs selectByPrimaryKey(String aboutUsId);

    int updateByPrimaryKeySelective(AboutUs record);

    int updateByPrimaryKeyWithBLOBs(AboutUs record);

    int updateByPrimaryKey(AboutUs record);
    
    /**
     * 查询关于我们内容
     * @return
     */
    List<AboutUs> selectAboutAsInfo();
    
}