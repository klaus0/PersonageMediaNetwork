package com.personage_media_network.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.entity.AboutUs;
import com.personage_media_network.service.AboutUsService;

@RestController
@RequestMapping("admin/aboutUs")
public class AboutUsAdminController {

	@Autowired
	private AboutUsService aboutUsService;

	/**
	 * 查询
	 * 
	 * @return
	 */
	@GetMapping("getAboutUsInfo")
	public ReturnCode getAboutUsInfo() {

		return aboutUsService.getAboutUsInfo();

	}

	/**
	 * 修改
	 * 
	 * @param aboutUs
	 * @return
	 */

	@PutMapping("updateAboutUsInfo")
	public ReturnCode updateAboutUsInfo(@RequestBody @Valid AboutUs aboutUs, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage()).object(aboutUs)
					.build();
		}

		return aboutUsService.updateAboutUsInfo(aboutUs);

	}

}
