package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;
/**
 * 角色表
 * @author klaus
 *
 */
public class SysRole  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String roleId;//角色ID

    private String roleName;//角色名称

    private String roleInfo;//角色描述

    private Date createTime;//创建时间

    private Date updateTime;//更新时间

    private Boolean isDelete;//是否删除

    private Boolean isEnable;//是否可用

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    public String getRoleInfo() {
        return roleInfo;
    }

    public void setRoleInfo(String roleInfo) {
        this.roleInfo = roleInfo == null ? null : roleInfo.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Boolean getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }
}