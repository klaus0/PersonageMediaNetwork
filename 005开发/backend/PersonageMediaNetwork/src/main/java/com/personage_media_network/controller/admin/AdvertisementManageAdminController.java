package com.personage_media_network.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.entity.AdvertisementManage;
import com.personage_media_network.httpbean.req.AdvertisementManageReq;
import com.personage_media_network.service.AdvertisementManageService;

@RestController
@RequestMapping("admin/advertisementManage")
public class AdvertisementManageAdminController {
	@Autowired
	private AdvertisementManageService advertisementManageService;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	@GetMapping("listAdvertisementManage")
	public ReturnCode listAdvertisementManage(@RequestBody AdvertisementManageReq advertisementManageReq) {

		return advertisementManageService.listAdvertisementManage(advertisementManageReq);
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PostMapping("saveAdvertisementManage")
	public ReturnCode saveAdvertisementManage(@RequestBody @Valid AdvertisementManage advertisementManage,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage())
					.object(advertisementManage).build();
		}

		return advertisementManageService.saveAdvertisementManage(advertisementManage);

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PutMapping("updateOrRemoveAdvertisementManage")
	public ReturnCode updateOrRemoveAdvertisementManage(@RequestBody @Valid AdvertisementManage advertisementManage,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage())
					.object(advertisementManage).build();
		}

		return advertisementManageService.updateOrRemoveAdvertisementManage(advertisementManage);

	}
}
