package com.personage_media_network.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.BusinessCooperation;
@Mapper
public interface BusinessCooperationMapper {
    int deleteByPrimaryKey(String businessCooperationId);

    int insert(BusinessCooperation record);

    int insertSelective(BusinessCooperation record);

    BusinessCooperation selectByPrimaryKey(String businessCooperationId);

    int updateByPrimaryKeySelective(BusinessCooperation record);

    int updateByPrimaryKey(BusinessCooperation record);

	int selectBusinessCooperationListCount(BusinessCooperation businessCooperation);

	List<BusinessCooperation> selectBusinessCooperationList(BusinessCooperation businessCooperation);
}