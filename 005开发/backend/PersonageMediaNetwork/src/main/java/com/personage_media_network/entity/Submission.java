package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 投稿与转载
 * 
 * @author klaus
 *
 */
public class Submission implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String submissionId;// 投稿与转载ID
	@NotBlank(message = "希望授权的渠道必填")
	private String submissionChannel;// 希望授权的渠道
	@NotBlank(message = "姓名必填")
	private String submissionUserName;// 投稿人姓名
	@Email(message = "请检查邮箱格式")
	private String submissionUserEmail;// 投稿人邮箱
	@Pattern(regexp = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$", message = "请检查手机格式")
	private String submissionUserPhone;// 投稿人手机

	private String submissionUserWechat;// 投稿人微信
	@NotBlank(message = "公司必填")
	private String submissionUserCompany;// 投稿人公司

	private String submissionUserMessage;// 投稿人留言

	private Date createTime;// 创建时间

	private Date updateTime;// 更新时间

	private Boolean isDelete;// 是否删除

	public String getSubmissionId() {
		return submissionId;
	}

	public void setSubmissionId(String submissionId) {
		this.submissionId = submissionId == null ? null : submissionId.trim();
	}

	public String getSubmissionChannel() {
		return submissionChannel;
	}

	public void setSubmissionChannel(String submissionChannel) {
		this.submissionChannel = submissionChannel == null ? null : submissionChannel.trim();
	}

	public String getSubmissionUserName() {
		return submissionUserName;
	}

	public void setSubmissionUserName(String submissionUserName) {
		this.submissionUserName = submissionUserName == null ? null : submissionUserName.trim();
	}

	public String getSubmissionUserEmail() {
		return submissionUserEmail;
	}

	public void setSubmissionUserEmail(String submissionUserEmail) {
		this.submissionUserEmail = submissionUserEmail == null ? null : submissionUserEmail.trim();
	}

	public String getSubmissionUserPhone() {
		return submissionUserPhone;
	}

	public void setSubmissionUserPhone(String submissionUserPhone) {
		this.submissionUserPhone = submissionUserPhone == null ? null : submissionUserPhone.trim();
	}

	public String getSubmissionUserWechat() {
		return submissionUserWechat;
	}

	public void setSubmissionUserWechat(String submissionUserWechat) {
		this.submissionUserWechat = submissionUserWechat == null ? null : submissionUserWechat.trim();
	}

	public String getSubmissionUserCompany() {
		return submissionUserCompany;
	}

	public void setSubmissionUserCompany(String submissionUserCompany) {
		this.submissionUserCompany = submissionUserCompany == null ? null : submissionUserCompany.trim();
	}

	public String getSubmissionUserMessage() {
		return submissionUserMessage;
	}

	public void setSubmissionUserMessage(String submissionUserMessage) {
		this.submissionUserMessage = submissionUserMessage == null ? null : submissionUserMessage.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
}