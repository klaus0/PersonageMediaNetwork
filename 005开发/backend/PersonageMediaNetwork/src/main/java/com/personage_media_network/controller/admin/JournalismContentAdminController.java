package com.personage_media_network.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.httpbean.req.JournalismContentReq;
import com.personage_media_network.service.JournalismContentService;

@RestController
@RequestMapping("admin/journalismContent")
public class JournalismContentAdminController {
	@Autowired
	private JournalismContentService journalismContentService;

	/**
	 * 查询
	 * 
	 * @return
	 */
	@GetMapping("getJournalismContent")
	public ReturnCode getJournalismContent(String journalismId) {

		return journalismContentService.getJournalismContent(journalismId);
	}

	// 新增

	@PostMapping("saveJournalismContent")
	public ReturnCode saveJournalismContent( @RequestBody @Valid JournalismContentReq journalismContentReq ,BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage())
					.object(journalismContentReq).build();
		}
		return journalismContentService.saveJournalismContent(journalismContentReq);

	}

	// 修改

	@PutMapping("updateOrRemoveJournalismContent")
	public ReturnCode updateOrRemoveJournalismContent(@RequestBody @Valid JournalismContentReq journalismContentReq,BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage())
					.object(journalismContentReq).build();
		}
		return journalismContentService.updateOrRemoveJournalismContent(journalismContentReq);

	}
}
