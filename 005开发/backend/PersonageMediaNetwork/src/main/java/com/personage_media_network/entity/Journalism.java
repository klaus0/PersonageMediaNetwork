package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;

/**
 * 资讯文章
 * 
 * @author klaus
 *
 */
public class Journalism implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String journalismId;// 资讯ID
	@NotBlank(message = "资讯归类必填")
	private String journalismBelonged;// 资讯归类
	@NotBlank(message = "资讯标题必填")
	private String journalismTitle;// 资讯标题
	@NotBlank(message = "资讯小标题必填")
	private String journalismSubTitle;// 资讯小标题
	@NotBlank(message = "资讯封面必填")
	private String journalismImage;// 资讯封面
	private Date journalismCreate;// 资讯时间
	@NotBlank(message = " 资讯类型必填")
	private String journalismType;// 资讯类型

	private Integer journalismAmountOfAccess;// 访问量

	private String journalismOperationUser;// 资讯发布人

	private Date updateTime;// 更新时间

	private Boolean isDelete;// 是否删除

	public String getJournalismId() {
		return journalismId;
	}

	public void setJournalismId(String journalismId) {
		this.journalismId = journalismId == null ? null : journalismId.trim();
	}

	public String getJournalismBelonged() {
		return journalismBelonged;
	}

	public void setJournalismBelonged(String journalismBelonged) {
		this.journalismBelonged = journalismBelonged == null ? null : journalismBelonged.trim();
	}

	public String getJournalismTitle() {
		return journalismTitle;
	}

	public void setJournalismTitle(String journalismTitle) {
		this.journalismTitle = journalismTitle == null ? null : journalismTitle.trim();
	}

	public String getJournalismSubTitle() {
		return journalismSubTitle;
	}

	public void setJournalismSubTitle(String journalismSubTitle) {
		this.journalismSubTitle = journalismSubTitle == null ? null : journalismSubTitle.trim();
	}

	public String getJournalismImage() {
		return journalismImage;
	}

	public void setJournalismImage(String journalismImage) {
		this.journalismImage = journalismImage == null ? null : journalismImage.trim();
	}

	public Date getJournalismCreate() {
		return journalismCreate;
	}

	public void setJournalismCreate(Date journalismCreate) {
		this.journalismCreate = journalismCreate;
	}

	public String getJournalismType() {
		return journalismType;
	}

	public void setJournalismType(String journalismType) {
		this.journalismType = journalismType == null ? null : journalismType.trim();
	}

	public Integer getJournalismAmountOfAccess() {
		return journalismAmountOfAccess;
	}

	public void setJournalismAmountOfAccess(Integer journalismAmountOfAccess) {
		this.journalismAmountOfAccess = journalismAmountOfAccess;
	}

	public String getJournalismOperationUser() {
		return journalismOperationUser;
	}

	public void setJournalismOperationUser(String journalismOperationUser) {
		this.journalismOperationUser = journalismOperationUser == null ? null : journalismOperationUser.trim();
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
}