package com.personage_media_network.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.SysUser;
@Mapper
public interface SysUserMapper {
    int deleteByPrimaryKey(String userId);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(String userId);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

	int selectSysUserListCount(SysUser sysUser);

	List<SysUser> selectSysUserList(SysUser sysUser);
	
	int checkUserLoginNameIsRepeat();
	
	SysUser selectByLoginName(String userLoginName);
	
}