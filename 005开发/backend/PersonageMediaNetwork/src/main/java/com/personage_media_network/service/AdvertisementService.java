package com.personage_media_network.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.AdvertisementMapper;
import com.personage_media_network.entity.Advertisement;
import com.personage_media_network.entity.PageBean;
import com.personage_media_network.httpbean.req.AdvertisementReq;

/**
 * 广告
 * 
 * @author klaus
 *
 */
@Service
public class AdvertisementService {
	@Autowired
	private AdvertisementMapper advertisementMapper;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	public ReturnCode listAdvertisement(AdvertisementReq advertisementReq) {

		PageBean<Advertisement> page = new PageBean<Advertisement>();
		page.setItemTotal(advertisementMapper.selectAdvertisementListCount(advertisementReq));
		page.setItems(advertisementMapper.selectAdvertisementList(advertisementReq));
		return new Builder().code(0).msg("查询成功").object(page).build();
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode saveAdvertisement(Advertisement advertisement) {

		advertisementMapper.insertSelective(advertisement);

		return new Builder().code(0).msg("保存成功").object(advertisement).build();

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode updateOrRemoveAdvertisement(Advertisement advertisement) {

		advertisementMapper.updateByPrimaryKeySelective(advertisement);

		return new Builder().code(0).msg("操作成功").object(advertisement).build();

	}

	/**
	 * 查询橱窗广告
	 * 
	 * @return
	 */
	public ReturnCode listAdvertisementShowcase() {
		List<Advertisement> advertisements = advertisementMapper.selectAdvertisementShowcaseList();
		return new Builder().code(0).msg("获取成功").object(advertisements).build();
	}

}
