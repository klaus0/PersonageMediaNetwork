package com.personage_media_network.httpbean.req;

import com.personage_media_network.entity.Paging;
import com.personage_media_network.entity.SysUser;

public class SysUserReq extends SysUser {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
 
	private Paging paging;

	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}
	
	
}
