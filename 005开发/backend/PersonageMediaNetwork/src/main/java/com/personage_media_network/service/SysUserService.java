package com.personage_media_network.service;

import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.SysUserMapper;
import com.personage_media_network.entity.PageBean;
import com.personage_media_network.entity.SysUser;
import com.personage_media_network.enums.ResultEnum;
import com.personage_media_network.exception.CustomException;
import com.personage_media_network.util.Tool;
import com.personage_media_network.util.WebTokenUtil;

/**
 * 用户管理
 * 
 * @author klaus
 *
 */
@Service
public class SysUserService {

	@Autowired
	private SysUserMapper sysUserMapper;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	public ReturnCode listSysUser(SysUser sysUser) {

		PageBean<SysUser> page = new PageBean<SysUser>();
		page.setItemTotal(sysUserMapper.selectSysUserListCount(sysUser));
		page.setItems(sysUserMapper.selectSysUserList(sysUser));
		return new Builder().code(0).msg("查询成功").object(page).build();
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode saveSysUser(SysUser sysUser) {

		sysUserMapper.insertSelective(sysUser);

		return new Builder().code(0).msg("保存成功").object(sysUser).build();

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode updateOrRemoveSysUser(SysUser sysUser) {

		sysUserMapper.updateByPrimaryKeySelective(sysUser);

		return new Builder().code(0).msg("操作成功").object(sysUser).build();

	}

	/**
	 * 登陆
	 * 
	 * @param sysUser
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public ReturnCode login(SysUser sysUser) throws UnsupportedEncodingException {

		if (sysUser == null || sysUser.getUserLoginName() == null || "".equals(sysUser.getUserLoginName())
				|| sysUser.getUserPassword() == null || "".equals(sysUser.getUserPassword())) {

			throw new CustomException(ResultEnum.UNKONW_ERROR);
		}

		// 判断用户是否存在
		SysUser reSysUser = sysUserMapper.selectByLoginName(sysUser.getUserLoginName());

		if (reSysUser == null) {

			throw new CustomException(ResultEnum.USER_NOT_EXSIT);
		}

		// 判断密码是否正确
		String md5Password = Tool.md5Encode(sysUser.getUserPassword());
		if (md5Password == null || !md5Password.equals(reSysUser.getUserPassword())) {

			throw new CustomException(ResultEnum.PASSWORD_ERR);

		}

		// 生成token
		String webtoken = WebTokenUtil.getSysUserWebToken(reSysUser);

		return new Builder().code(0).msg("登陆成功").object(webtoken).build();
	}

}
