package com.personage_media_network.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.entity.Submission;
import com.personage_media_network.httpbean.req.SubmissionReq;
import com.personage_media_network.service.SubmissionService;

@RestController
@RequestMapping("admin/submission")
public class SubmissionAdminController {
	@Autowired
	private SubmissionService submissionService;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	@GetMapping("listSubmission")
	public ReturnCode listSubmission(@RequestBody SubmissionReq SubmissionReq) {

		return submissionService.listSubmission(SubmissionReq);
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PostMapping("saveSubmission")
	public ReturnCode saveSubmission(@RequestBody @Valid Submission Submission, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage()).object(Submission)
					.build();
		}
		return submissionService.saveSubmission(Submission);

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PutMapping("updateOrRemoveSubmission")
	public ReturnCode updateOrRemoveSubmission(@RequestBody @Valid Submission Submission, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage()).object(Submission)
					.build();
		}
		return submissionService.updateOrRemoveSubmission(Submission);

	}
}
