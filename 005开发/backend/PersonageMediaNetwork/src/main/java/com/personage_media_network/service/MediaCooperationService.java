package com.personage_media_network.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.dao.MediaCooperationMapper;
import com.personage_media_network.entity.MediaCooperation;
import com.personage_media_network.entity.PageBean;

/**
 * 媒体合作
 * @author klaus
 *
 */
@Service
public class MediaCooperationService {

	@Autowired
	private MediaCooperationMapper mediaCooperationMapper;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	public ReturnCode listMediaCooperation(MediaCooperation mediaCooperation) {

		PageBean<MediaCooperation> page = new PageBean<MediaCooperation>();
		page.setItemTotal(mediaCooperationMapper.selectMediaCooperationListCount(mediaCooperation));
		page.setItems(mediaCooperationMapper.selectMediaCooperationList(mediaCooperation));
		return new Builder().code(0).msg("查询成功").object(page).build();
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode saveMediaCooperation(MediaCooperation mediaCooperation) {

		mediaCooperationMapper.insertSelective(mediaCooperation);

		return new Builder().code(0).msg("保存成功").object(mediaCooperation).build();

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@Transactional
	public ReturnCode updateOrRemoveMediaCooperation(MediaCooperation mediaCooperation) {

		mediaCooperationMapper.updateByPrimaryKeySelective(mediaCooperation);

		return new Builder().code(0).msg("操作成功").object(mediaCooperation).build();

	}
	
}
