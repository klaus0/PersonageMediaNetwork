package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;

/**
 * 咨询详情
 * 
 * @author klaus
 *
 */
public class JournalismContent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String journalismContentId;// 资讯详情ID

	private String journalismId;// 资讯ID
	@NotBlank(message = "资讯详情媒体路径必填")
	private String journalismContentMedia;// 资讯详情媒体路径

	private String journalismContentBackMusic;// 资讯详情背景音乐

	private Date createTime;// 创建时间

	private Date updateTime;// 更新时间

	private Boolean isDelete;// 是否删除
	@NotBlank(message = "资讯详情内容不能为空")
	private String journalismContent;// 资讯详情内容

	public String getJournalismContentId() {
		return journalismContentId;
	}

	public void setJournalismContentId(String journalismContentId) {
		this.journalismContentId = journalismContentId == null ? null : journalismContentId.trim();
	}

	public String getJournalismId() {
		return journalismId;
	}

	public void setJournalismId(String journalismId) {
		this.journalismId = journalismId == null ? null : journalismId.trim();
	}

	public String getJournalismContentMedia() {
		return journalismContentMedia;
	}

	public void setJournalismContentMedia(String journalismContentMedia) {
		this.journalismContentMedia = journalismContentMedia == null ? null : journalismContentMedia.trim();
	}

	public String getJournalismContentBackMusic() {
		return journalismContentBackMusic;
	}

	public void setJournalismContentBackMusic(String journalismContentBackMusic) {
		this.journalismContentBackMusic = journalismContentBackMusic == null ? null : journalismContentBackMusic.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getJournalismContent() {
		return journalismContent;
	}

	public void setJournalismContent(String journalismContent) {
		this.journalismContent = journalismContent == null ? null : journalismContent.trim();
	}
}