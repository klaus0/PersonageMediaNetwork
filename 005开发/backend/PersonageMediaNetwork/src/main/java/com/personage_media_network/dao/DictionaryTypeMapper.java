package com.personage_media_network.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.DictionaryType;
@Mapper
public interface DictionaryTypeMapper {
    int deleteByPrimaryKey(String dictionaryTypeId);

    int insert(DictionaryType record);

    int insertSelective(DictionaryType record);

    DictionaryType selectByPrimaryKey(String dictionaryTypeId);

    int updateByPrimaryKeySelective(DictionaryType record);

    int updateByPrimaryKey(DictionaryType record);

	List<DictionaryType> listDetectionType();
	
	int selectMaxDictionaryTypeSort();
	
	int selectCheckDictionaryTypeNameRepeat(String dictionaryTypeName);
	
	int selectCheckDictionaryTypePymRepeat(String dictionaryTypePym);
	
	DictionaryType selectDictionaryTypeByDictionaryTypePym(String dictionaryTypePym);
	
	
}