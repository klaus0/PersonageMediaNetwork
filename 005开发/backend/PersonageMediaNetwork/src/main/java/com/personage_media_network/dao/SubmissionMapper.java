package com.personage_media_network.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.Submission;
@Mapper
public interface SubmissionMapper {
    int deleteByPrimaryKey(String submissionId);

    int insert(Submission record);

    int insertSelective(Submission record);

    Submission selectByPrimaryKey(String submissionId);

    int updateByPrimaryKeySelective(Submission record);

    int updateByPrimaryKey(Submission record);

	int selectSubmissionListCount(Submission submission);

	List<Submission> selectSubmissionList(Submission submission);
}