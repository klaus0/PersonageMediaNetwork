package com.personage_media_network.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.DictionaryItems;
import com.personage_media_network.entity.DictionaryType;

@Mapper
public interface DictionaryItemsMapper {
	int deleteByPrimaryKey(String dictionaryItemId);

	int insert(DictionaryItems record);

	int insertSelective(DictionaryItems record);

	DictionaryItems selectByPrimaryKey(String dictionaryItemId);

	int updateByPrimaryKeySelective(DictionaryItems record);

	int updateByPrimaryKey(DictionaryItems record);

	List<DictionaryItems> listDictionaryItems(String dictionaryTypeId);

	int selectMaxDictionaryItemSort(String dictionaryTypeId);

	int selectCheckDictionaryItemNameRepeat(DictionaryItems dictionaryItems);

	int selectCheckDictionaryItemPymRepeat(DictionaryItems dictionaryItems);

	List<DictionaryItems> selectDictionaryItemByDictionaryPym(String dictionaryTypePym);

}