package com.personage_media_network.entity;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;

/**
 * 加入我们介绍
 * 
 * @author klaus
 *
 */
public class JoinUs  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String joinUsId;// 加入我们ID
	
	private Date createTime;// 创建时间

	private Date updateTime;// 更新时间
@NotBlank(message="介绍内容不能为空")
	private String joidUsContent;// 加入我们介绍内容

	public String getJoinUsId() {
		return joinUsId;
	}

	public void setJoinUsId(String joinUsId) {
		this.joinUsId = joinUsId == null ? null : joinUsId.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getJoidUsContent() {
		return joidUsContent;
	}

	public void setJoidUsContent(String joidUsContent) {
		this.joidUsContent = joidUsContent == null ? null : joidUsContent.trim();
	}
}