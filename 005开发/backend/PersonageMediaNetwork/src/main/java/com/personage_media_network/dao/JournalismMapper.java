package com.personage_media_network.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.Journalism;
import com.personage_media_network.httpbean.req.JournalismReq;

@Mapper
public interface JournalismMapper {
	int deleteByPrimaryKey(String journalismId);

	int insert(Journalism record);

	int insertSelective(Journalism record);

	Journalism selectByPrimaryKey(String journalismId);

	int updateByPrimaryKeySelective(Journalism record);

	int updateByPrimaryKey(Journalism record);

	// 分页查询

	List<Journalism> selectJournalismList(JournalismReq journalismReq);

	// 分页查询数目

	int selectJournalismListCount(JournalismReq journalismReq);

}