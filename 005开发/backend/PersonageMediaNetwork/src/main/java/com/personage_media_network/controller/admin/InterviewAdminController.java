package com.personage_media_network.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.entity.Interview;
import com.personage_media_network.httpbean.req.InterviewReq;
import com.personage_media_network.service.InterviewService;

@RestController
@RequestMapping("admin/interview")
public class InterviewAdminController {
	@Autowired
	private InterviewService interviewService;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	@GetMapping("listInterview")
	public ReturnCode listInterview(@RequestBody InterviewReq InterviewReq) {

		return interviewService.listInterview(InterviewReq);
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PostMapping("saveInterview")
	public ReturnCode saveInterview(@RequestBody @Valid Interview Interview, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage()).object(Interview)
					.build();
		}

		return interviewService.saveInterview(Interview);

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PutMapping("updateOrRemoveInterview")
	public ReturnCode updateOrRemoveInterview(@RequestBody @Valid Interview Interview, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage()).object(Interview)
					.build();
		}

		return interviewService.updateOrRemoveInterview(Interview);

	}
}
