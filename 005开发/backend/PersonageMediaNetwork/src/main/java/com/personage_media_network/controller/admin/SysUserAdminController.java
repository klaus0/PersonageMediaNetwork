package com.personage_media_network.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personage_media_network.ReturnCode;
import com.personage_media_network.ReturnCode.Builder;
import com.personage_media_network.entity.SysUser;
import com.personage_media_network.httpbean.req.SysUserReq;
import com.personage_media_network.service.SysUserService;

@RestController
@RequestMapping("admin/sysUser")
public class SysUserAdminController {
	@Autowired
	private SysUserService sysUserService;

	/**
	 * 分页查询
	 * 
	 * @param advertisementManageReq
	 * @return
	 */
	@GetMapping("listSysUser")
	public ReturnCode listSysUser(@RequestBody SysUserReq SysUserReq) {

		return sysUserService.listSysUser(SysUserReq);
	}

	/**
	 * 新增
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PostMapping("saveSysUser")
	public ReturnCode saveSysUser(@RequestBody @Valid SysUser SysUser, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage()).object(SysUser)
					.build();
		}

		return sysUserService.saveSysUser(SysUser);

	}

	/**
	 * 修改
	 * 
	 * @param advertisementManage
	 * @return
	 */
	@PutMapping("updateOrRemoveSysUser")
	public ReturnCode updateOrRemoveSysUser(@RequestBody SysUser SysUser, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			// 获取错误信息，并打印
			System.out.println(bindingResult.getFieldError().getDefaultMessage());
			// 禁止其继续执行

			return new Builder().code(-1).msg(bindingResult.getFieldError().getDefaultMessage()).object(SysUser)
					.build();
		}
		return sysUserService.updateOrRemoveSysUser(SysUser);

	}
}
