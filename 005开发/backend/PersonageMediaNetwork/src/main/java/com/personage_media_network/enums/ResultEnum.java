package com.personage_media_network.enums;

/**
 * 统一错误记录
 * 
 * @author klaus
 *
 *  -1位置错误 0成功 300验证
 */
public enum ResultEnum implements IResultEnumRole {
	UNKONW_ERROR(-1, "未知错误"),
	SUCCESS(0, "成功"),
	NO_INFORMATION(300, "未查询信息"),
	USER_NOT_EXSIT(301, "用户不存在"),
	PASSWORD_ERR(302,"密码错误"),
	PYM_REPEAT(303,"添加字典类型Pym重复"),
	NAME_REPEAT(304,"添加字典类型名称重复");

	private Integer code;

	private String msg;

	ResultEnum(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	@Override
	public Integer getCode() {
		return code;
	}

	@Override
	public String getMsg() {
		return msg;
	}
}
