package com.personage_media_network.dao;

import org.apache.ibatis.annotations.Mapper;

import com.personage_media_network.entity.SysModule;
@Mapper
public interface SysModuleMapper {
    int deleteByPrimaryKey(String moduleId);

    int insert(SysModule record);

    int insertSelective(SysModule record);

    SysModule selectByPrimaryKey(String moduleId);

    int updateByPrimaryKeySelective(SysModule record);

    int updateByPrimaryKey(SysModule record);
}